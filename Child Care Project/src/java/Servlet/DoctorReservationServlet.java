/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet;

import Model.Account;
import Model.Children;
import Model.Reservation2;
import dal.DAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 *
 * @author duchi
 */
@WebServlet(name = "DoctorReservationServlet", urlPatterns = {"/DoctorReservation"})
public class DoctorReservationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAO dao = new DAO();
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("ACCOUNT");

        if (account == null) {

            request.setAttribute("LOGIN_VALID", "Please Login First");
            RequestDispatcher rd = request.getRequestDispatcher("Login_inner_staff.jsp");
            rd.forward(request, response);
        } else if (account.getRoleId() == 3) {

            ArrayList<Reservation2> reservation_list = dao.get_reservation_by_staffId(account.getAccountId());
            ArrayList<Children> children_list = dao.get_children_list();

            request.setAttribute("CHILDREN_LIST", children_list);
            request.setAttribute("RESERVATION_LIST", reservation_list);
            RequestDispatcher rd = request.getRequestDispatcher("DoctorReservation_inner.jsp");
            rd.forward(request, response);

        } else {
            response.sendRedirect("Home");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAO dao = new DAO();
        HttpSession session = request.getSession();

        //lay ra account
        Account account = (Account) session.getAttribute("ACCOUNT");
        if (account == null) {
            response.sendRedirect("Login?type=staff");
        }

        //lay ra list
        ArrayList<Reservation2> reservation_list = dao.get_reservation_by_staffId(account.getAccountId());
        ArrayList<Children> children_list = dao.get_children_list();

        //new list
        ArrayList<Reservation2> status_list = new ArrayList<>();
        ArrayList<Reservation2> search_list = new ArrayList<>();
        ArrayList<Reservation2> filter_list = reservation_list;

        //lay ra type
        String type = request.getParameter("txtType");

        if (type.equals("filter")) {
            String filter = request.getParameter("txtFilter");
            if (filter.equals("1")) { // newest first
                Collections.sort(filter_list, Comparator.comparing(Reservation2::getDate).reversed());
            } else if (filter.equals("2")) { // oldest first
                Collections.sort(filter_list, Comparator.comparing(Reservation2::getDate));
            }
            request.setAttribute("CHILDREN_LIST", children_list);
            request.setAttribute("RESERVATION_LIST", filter_list);
            RequestDispatcher rd = request.getRequestDispatcher("DoctorReservation_inner.jsp");
            rd.forward(request, response);
        } else if (type.equals("search")) {
            String search = request.getParameter("txtSearch").toLowerCase();

            for (Reservation2 reservation : reservation_list) {
                for (Children children : children_list) {
                    if (children.getChildrenId() == reservation.getChildrenId()) {
                        if (children.getName().toLowerCase().contains(search)) {
                            search_list.add(reservation);
                        }
                    }
                }
            }

            request.setAttribute("CHILDREN_LIST", children_list);
            request.setAttribute("RESERVATION_LIST", search_list);
            RequestDispatcher rd = request.getRequestDispatcher("DoctorReservation_inner.jsp");
            rd.forward(request, response);
        } else if (type.equals("status")) {
            int status = Integer.parseInt(request.getParameter("txtStatus"));
            switch (status) {
                case 0: {
                    request.setAttribute("CHILDREN_LIST", children_list);
                    request.setAttribute("RESERVATION_LIST", reservation_list);
                    RequestDispatcher rd = request.getRequestDispatcher("DoctorReservation_inner.jsp");
                    rd.forward(request, response);
                    break;
                }
                case 1: {

                    for (Reservation2 reservation : reservation_list) {
                        if (reservation.getStatusId() == 1) {
                            status_list.add(reservation);
                        }
                    }
                    request.setAttribute("CHILDREN_LIST", children_list);
                    request.setAttribute("RESERVATION_LIST", status_list);
                    RequestDispatcher rd = request.getRequestDispatcher("DoctorReservation_inner.jsp");
                    rd.forward(request, response);
                    break;
                }
                case 2: {

                    for (Reservation2 reservation : reservation_list) {
                        if (reservation.getStatusId() == 2) {
                            status_list.add(reservation);
                        }
                    }
                    request.setAttribute("CHILDREN_LIST", children_list);
                    request.setAttribute("RESERVATION_LIST", status_list);
                    RequestDispatcher rd = request.getRequestDispatcher("DoctorReservation_inner.jsp");
                    rd.forward(request, response);
                    break;
                }
                case 3: {

                    for (Reservation2 reservation : reservation_list) {
                        if (reservation.getStatusId() == 3) {
                            status_list.add(reservation);
                        }
                    }
                    request.setAttribute("CHILDREN_LIST", children_list);
                    request.setAttribute("RESERVATION_LIST", status_list);
                    RequestDispatcher rd = request.getRequestDispatcher("DoctorReservation_inner.jsp");
                    rd.forward(request, response);
                    break;
                }
                case 4: {

                    for (Reservation2 reservation : reservation_list) {
                        if (reservation.getStatusId() == 4) {
                            status_list.add(reservation);
                        }
                    }
                    request.setAttribute("CHILDREN_LIST", children_list);
                    request.setAttribute("RESERVATION_LIST", status_list);
                    RequestDispatcher rd = request.getRequestDispatcher("DoctorReservation_inner.jsp");
                    rd.forward(request, response);
                    break;
                }
                case 5: {

                    for (Reservation2 reservation : reservation_list) {
                        if (reservation.getStatusId() == 5) {
                            status_list.add(reservation);
                        }
                    }
                    request.setAttribute("CHILDREN_LIST", children_list);
                    request.setAttribute("RESERVATION_LIST", status_list);
                    RequestDispatcher rd = request.getRequestDispatcher("DoctorReservation_inner.jsp");
                    rd.forward(request, response);
                    break;
                }
                case 6: {

                    for (Reservation2 reservation : reservation_list) {
                        if (reservation.getStatusId() == 6) {
                            status_list.add(reservation);
                        }
                    }
                    request.setAttribute("CHILDREN_LIST", children_list);
                    request.setAttribute("RESERVATION_LIST", status_list);
                    RequestDispatcher rd = request.getRequestDispatcher("DoctorReservation_inner.jsp");
                    rd.forward(request, response);
                    break;
                }
            }
            return;
        } else {
            response.sendRedirect("DoctorReservation");
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
