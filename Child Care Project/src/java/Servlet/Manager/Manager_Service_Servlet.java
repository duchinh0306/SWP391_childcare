/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet.Manager;

import Model.*;
import dal.DAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 *
 * @author BlackZ36
 */
@WebServlet(name = "Manager_Service_Servlet", urlPatterns = {"/Manager/Services"})
public class Manager_Service_Servlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        DAO dao = new DAO();

        //get account
        Account account = (Account) session.getAttribute("ACCOUNT");
//        if(account == null || account.getRoleId() != 4){
//            response.sendRedirect("http://localhost:8080/Child_Care_Project/");
//        }

        //get service list
        ArrayList<Services> service_list = dao.admin_get_service_list();
        
        //get category list
        ArrayList<ServicesCategory> category_list = dao.get_service_category_list();
        
        //set attribute
        request.setAttribute("SERVICE_LIST", service_list);
        request.setAttribute("CATEGORY_LIST", category_list);
        
        //send
        RequestDispatcher rd = request.getRequestDispatcher("Services_inner.jsp");
        rd.forward(request, response);
    }    
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
}
