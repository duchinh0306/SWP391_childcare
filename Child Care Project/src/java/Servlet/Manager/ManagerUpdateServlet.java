/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet.Manager;

import Model.Account;
import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author duchi
 */
@WebServlet(name = "ManagerUpdateServlet", urlPatterns = {"/Manager/ManagerUpdate"})
public class ManagerUpdateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("http://localhost:8080/Child_Care_Project/");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        DAO dao = new DAO();

        //check
        Account account = (Account) session.getAttribute("ACCOUNT");
        if (account.getRoleId() != 4) {
            response.sendRedirect("http://localhost:8080/Child_Care_Project/");
            return;
        }

        //lay prm
        String reservationId = request.getParameter("txtID");
        String status = request.getParameter("txtStatus");

        dao.update_reservation_status(Integer.parseInt(status), Integer.parseInt(reservationId));
        response.sendRedirect("ReservationDetail?reservationId=" + reservationId);

    }

}
