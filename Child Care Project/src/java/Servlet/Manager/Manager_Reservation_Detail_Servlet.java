/*
     * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
     * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet.Manager;

import Model.*;
import dal.DAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 *
 * @author duchi
 */
@WebServlet(name = "Manager_Reservation_Detail_Servlet", urlPatterns = {"/Manager/ReservationDetail"})
public class Manager_Reservation_Detail_Servlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAO dao = new DAO();
        HttpSession session = request.getSession();

        //lay ra account
        Account account = (Account) session.getAttribute("ACCOUNT");

        //check
        if (account.getRoleId() != 4) {
            response.sendRedirect("http://localhost:8080/Child_Care_Project/");
            return;
        }
        
        //lay ra reservation id
        String reservationId = request.getParameter("reservationId");
        if(reservationId == null){
            response.sendRedirect("http://localhost:8080/Child_Care_Project/Manager/Reservation");
            return;
        }
        
        int id = Integer.parseInt(reservationId);
        
        //lay ra attribute
        Reservation2 reservation = dao.get_reservation_by_id(id);
        
        if(reservation == null){
            response.sendRedirect("http://localhost:8080/Child_Care_Project/Manager/Reservation");
            return;
        }
        
        Account doctor = dao.get_staff_by_id(reservation.getStaffId());
        Account patient = dao.get_patient_by_id(reservation.getPatientId());
        Children children = dao.get_children_by_id(reservation.getChildrenId());
        ArrayList<Services> service_list = dao.get_service_list();
        ArrayList<ReservationDetail2> reservatilDetail_list = dao.get_reservation_detail_by_id(reservation.getReservationId());
        
        //set attribute
        request.setAttribute("DOCTOR", doctor);
        request.setAttribute("CHILDREN", children);
        request.setAttribute("PATIENT", patient);
        request.setAttribute("RESERVATION", reservation);
        request.setAttribute("SERVICE_LIST", service_list);
        request.setAttribute("RESERVATION_DETAIL", reservatilDetail_list);
        
        RequestDispatcher rd = request.getRequestDispatcher("ReservationDetail_inner.jsp");
        rd.forward(request, response);
        
        

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
