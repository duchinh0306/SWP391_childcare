/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet.Manager;

import Model.Account;
import Model.Children;
import Model.Reservation2;
import Model.Services;
import dal.DAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author duchi
 */
@WebServlet(name = "ManagerSearchServlet", urlPatterns = {"/Manager/Search"})
public class ManagerSearchServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManagerSearchServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManagerSearchServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAO dao = new DAO();
        HttpSession session = request.getSession();

        //check
        Account account = (Account) session.getAttribute("ACCOUNT");
        if (account.getRoleId() != 4) {
            response.sendRedirect("http://localhost:8080/Child_Care_Project/");
            return;
        }

        //get prm
        String type = request.getParameter("txtType");
        ArrayList<Reservation2> reservation_list = dao.get_reservation_list();
        ArrayList<Account> patient_list = dao.admin_get_patients();

        if (type.equals("search")) {
            String searchType = request.getParameter("txtSearchType");
            String search = request.getParameter("txtSearch");

            if (searchType.equals("1")) { //search reservation by patient id
                try {
                    ArrayList<Reservation2> filteredReservations = new ArrayList<>();
                    for (Reservation2 reservation : reservation_list) {
                        if (reservation.getPatientId() == Integer.parseInt(search)) {
                            filteredReservations.add(reservation);
                        }
                    }

                    //get service list
                    ArrayList<Services> service_list = dao.get_service_list();

                    //get doctor list
                    ArrayList<Account> doctor_list = dao.admin_get_staffs();

                    //get child list
                    ArrayList<Children> children_list = dao.get_children_list();

                    request.setAttribute("DOCTOR_LIST", doctor_list);
                    request.setAttribute("CHILDREN_LIST", children_list);
                    request.setAttribute("SERVICE_LIST", service_list);
                    request.setAttribute("PATIENT_LIST", patient_list);
                    request.setAttribute("RESERVATION_LIST", filteredReservations);
                    RequestDispatcher rd = request.getRequestDispatcher("Reservation_inner.jsp");
                    rd.forward(request, response);

                } catch (Exception e) {
                    request.setAttribute("RESERVATION_LIST", reservation_list);
                    RequestDispatcher rd = request.getRequestDispatcher("Reservation_inner.jsp");
                    rd.forward(request, response);
                }
            } else if (searchType.equals("2")) { //search reservation by name
                try {
                    ArrayList<Reservation2> filteredReservations = new ArrayList<>();

                    for (Reservation2 reservation : reservation_list) {
                        for (Account patient : patient_list) {
                            if (reservation.getPatientId() == patient.getAccountId()) {
                                if (patient.getName().toLowerCase().contains(search.toLowerCase())) {
                                    filteredReservations.add(reservation);
                                }
                            }
                        }
                    }

                    //get list
                    ArrayList<Services> service_list = dao.get_service_list();
                    ArrayList<Account> doctor_list = dao.admin_get_staffs();
                    ArrayList<Children> children_list = dao.get_children_list();

                    request.setAttribute("DOCTOR_LIST", doctor_list);
                    request.setAttribute("CHILDREN_LIST", children_list);
                    request.setAttribute("SERVICE_LIST", service_list);
                    request.setAttribute("PATIENT_LIST", patient_list);
                    request.setAttribute("RESERVATION_LIST", filteredReservations);
                    RequestDispatcher rd = request.getRequestDispatcher("Reservation_inner.jsp");
                    rd.forward(request, response);

                } catch (Exception e) {
                    request.setAttribute("RESERVATION_LIST", reservation_list);
                    RequestDispatcher rd = request.getRequestDispatcher("Reservation_inner.jsp");
                    rd.forward(request, response);
                }
            } else {
                response.sendRedirect("http://localhost:8080/Child_Care_Project/Manager/Reservation");
                return;
            }

        } else if (type.equals("sort")) {
            String sort = request.getParameter("txtSort");
            if (sort.equals("1")) { // sort by latest date

                // Sắp xếp danh sách reservation_list theo ngày cập nhật mới nhất
                reservation_list.sort(new Comparator<Reservation2>() {
                    @Override
                    public int compare(Reservation2 reservation1, Reservation2 reservation2) {
                        return reservation2.getDate().compareTo(reservation1.getDate());
                    }
                });

                ArrayList<Services> service_list = dao.get_service_list();
                ArrayList<Account> doctor_list = dao.admin_get_staffs();
                ArrayList<Children> children_list = dao.get_children_list();

                request.setAttribute("DOCTOR_LIST", doctor_list);
                request.setAttribute("CHILDREN_LIST", children_list);
                request.setAttribute("SERVICE_LIST", service_list);
                request.setAttribute("PATIENT_LIST", patient_list);
                request.setAttribute("RESERVATION_LIST", reservation_list);

                RequestDispatcher rd = request.getRequestDispatcher("Reservation_inner.jsp");
                rd.forward(request, response);
            } else if (sort.equals("2")) { // sort by oldest date

                // Sắp xếp danh sách reservation_list theo ngày cập nhật cũ nhất
                reservation_list.sort(new Comparator<Reservation2>() {
                    @Override
                    public int compare(Reservation2 reservation1, Reservation2 reservation2) {
                        return reservation1.getDate().compareTo(reservation2.getDate());
                    }
                });

                ArrayList<Services> service_list = dao.get_service_list();
                ArrayList<Account> doctor_list = dao.admin_get_staffs();
                ArrayList<Children> children_list = dao.get_children_list();

                request.setAttribute("DOCTOR_LIST", doctor_list);
                request.setAttribute("CHILDREN_LIST", children_list);
                request.setAttribute("SERVICE_LIST", service_list);
                request.setAttribute("PATIENT_LIST", patient_list);
                request.setAttribute("RESERVATION_LIST", reservation_list);

                RequestDispatcher rd = request.getRequestDispatcher("Reservation_inner.jsp");
                rd.forward(request, response);
            } else if (sort.equals("3")) { // sort by total low to high
                // Sắp xếp danh sách reservation_list theo trường total từ thấp đến cao
                reservation_list.sort(new Comparator<Reservation2>() {
                    @Override
                    public int compare(Reservation2 reservation1, Reservation2 reservation2) {
                        return Double.compare(reservation1.getTotal(), reservation2.getTotal());
                    }
                });

                ArrayList<Services> service_list = dao.get_service_list();
                ArrayList<Account> doctor_list = dao.admin_get_staffs();
                ArrayList<Children> children_list = dao.get_children_list();

                request.setAttribute("DOCTOR_LIST", doctor_list);
                request.setAttribute("CHILDREN_LIST", children_list);
                request.setAttribute("SERVICE_LIST", service_list);
                request.setAttribute("PATIENT_LIST", patient_list);
                request.setAttribute("RESERVATION_LIST", reservation_list);

                RequestDispatcher rd = request.getRequestDispatcher("Reservation_inner.jsp");
                rd.forward(request, response);
            } else if (sort.equals("4")) { //sort by total high to low

                reservation_list.sort(new Comparator<Reservation2>() {
                    @Override
                    public int compare(Reservation2 reservation1, Reservation2 reservation2) {
                        return Double.compare(reservation2.getTotal(), reservation1.getTotal());
                    }
                });

                ArrayList<Services> service_list = dao.get_service_list();
                ArrayList<Account> doctor_list = dao.admin_get_staffs();
                ArrayList<Children> children_list = dao.get_children_list();

                request.setAttribute("DOCTOR_LIST", doctor_list);
                request.setAttribute("CHILDREN_LIST", children_list);
                request.setAttribute("SERVICE_LIST", service_list);
                request.setAttribute("PATIENT_LIST", patient_list);
                request.setAttribute("RESERVATION_LIST", reservation_list);

                RequestDispatcher rd = request.getRequestDispatcher("Reservation_inner.jsp");
                rd.forward(request, response);
            } else if (sort.equals("5")) { // sort by status
                // Sắp xếp danh sách reservation_list theo trạng thái từ thấp đến cao
                reservation_list.sort(new Comparator<Reservation2>() {
                    @Override
                    public int compare(Reservation2 reservation1, Reservation2 reservation2) {
                        return Integer.compare(reservation1.getStatusId(), reservation2.getStatusId());
                    }
                });

                ArrayList<Services> service_list = dao.get_service_list();
                ArrayList<Account> doctor_list = dao.admin_get_staffs();
                ArrayList<Children> children_list = dao.get_children_list();

                request.setAttribute("DOCTOR_LIST", doctor_list);
                request.setAttribute("CHILDREN_LIST", children_list);
                request.setAttribute("SERVICE_LIST", service_list);
                request.setAttribute("PATIENT_LIST", patient_list);
                request.setAttribute("RESERVATION_LIST", reservation_list);

                RequestDispatcher rd = request.getRequestDispatcher("Reservation_inner.jsp");
                rd.forward(request, response);
            } else if (sort.equals("6")) { //sort by patient name
                // Sắp xếp danh sách reservation_list theo tên của bệnh nhân
                reservation_list.sort(new Comparator<Reservation2>() {
                    @Override
                    public int compare(Reservation2 reservation1, Reservation2 reservation2) {
                        String patientName1 = getPatientNameById(reservation1.getPatientId());
                        String patientName2 = getPatientNameById(reservation2.getPatientId());
                        return patientName1.compareToIgnoreCase(patientName2);
                    }

                    private String getPatientNameById(int patientId) {
                        for (Account patient : patient_list) {
                            if (patient.getAccountId() == patientId) {
                                return patient.getName();
                            }
                        }
                        return ""; // Trả về chuỗi trống nếu không tìm thấy tên của bệnh nhân
                    }
                });

                ArrayList<Services> service_list = dao.get_service_list();
                ArrayList<Account> doctor_list = dao.admin_get_staffs();
                ArrayList<Children> children_list = dao.get_children_list();

                request.setAttribute("DOCTOR_LIST", doctor_list);
                request.setAttribute("CHILDREN_LIST", children_list);
                request.setAttribute("SERVICE_LIST", service_list);
                request.setAttribute("PATIENT_LIST", patient_list);
                request.setAttribute("RESERVATION_LIST", reservation_list);

                RequestDispatcher rd = request.getRequestDispatcher("Reservation_inner.jsp");
                rd.forward(request, response);
            } else if (sort.equals("7")) { //sort by reservation id

                reservation_list.sort(new Comparator<Reservation2>() {
                    @Override
                    public int compare(Reservation2 reservation1, Reservation2 reservation2) {
                        return Integer.compare(reservation1.getReservationId(), reservation2.getReservationId());
                    }
                });

                ArrayList<Services> service_list = dao.get_service_list();
                ArrayList<Account> doctor_list = dao.admin_get_staffs();
                ArrayList<Children> children_list = dao.get_children_list();

                request.setAttribute("DOCTOR_LIST", doctor_list);
                request.setAttribute("CHILDREN_LIST", children_list);
                request.setAttribute("SERVICE_LIST", service_list);
                request.setAttribute("PATIENT_LIST", patient_list);
                request.setAttribute("RESERVATION_LIST", reservation_list);

                RequestDispatcher rd = request.getRequestDispatcher("Reservation_inner.jsp");
                rd.forward(request, response);
            } else if (sort.equals("8")) { //sort by reservation id

                reservation_list.sort(new Comparator<Reservation2>() {
                    @Override
                    public int compare(Reservation2 reservation1, Reservation2 reservation2) {
                        return Integer.compare(reservation2.getReservationId(), reservation1.getReservationId());
                    }
                });

                ArrayList<Services> service_list = dao.get_service_list();
                ArrayList<Account> doctor_list = dao.admin_get_staffs();
                ArrayList<Children> children_list = dao.get_children_list();

                request.setAttribute("DOCTOR_LIST", doctor_list);
                request.setAttribute("CHILDREN_LIST", children_list);
                request.setAttribute("SERVICE_LIST", service_list);
                request.setAttribute("PATIENT_LIST", patient_list);
                request.setAttribute("RESERVATION_LIST", reservation_list);

                RequestDispatcher rd = request.getRequestDispatcher("Reservation_inner.jsp");
                rd.forward(request, response);
            } else {
                response.sendRedirect("http://localhost:8080/Child_Care_Project/Manager/Reservation");
                return;
            }

        } else {
            response.sendRedirect("http://localhost:8080/Child_Care_Project/Manager/Reservation");
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
