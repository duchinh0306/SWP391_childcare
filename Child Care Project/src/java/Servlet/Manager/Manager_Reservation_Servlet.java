/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet.Manager;

import Model.*;
import dal.DAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 *
 * @author BlackZ36
 */
@WebServlet(name = "Manager_Reservation_Servlet", urlPatterns = {"/Manager/Reservation"})
public class Manager_Reservation_Servlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        DAO dao = new DAO();

        //acount
        Account account = (Account) session.getAttribute("ACCOUNT");

        //check
        if (account == null) {
            response.sendRedirect("/Child_Care_Project/Home");
        } else if (account.getRoleId() != 4) {
            response.sendRedirect("/Child_Care_Project/Home");
        }
        
        //get reservation list
        ArrayList<Reservation2> reservation_list = dao.get_reservation_list();
        
        //get service list
        ArrayList<Services> service_list = dao.get_service_list();
        
        //get patient list
        ArrayList<Account> patient_list = dao.admin_get_patients();
        
        //get doctor list
        ArrayList<Account> doctor_list = dao.admin_get_staffs();
        
        //get child list
        ArrayList<Children> children_list = dao.get_children_list();
               
        
        //set attribute
        request.setAttribute("DOCTOR_LIST", doctor_list);
        request.setAttribute("CHILDREN_LIST", children_list);
        request.setAttribute("RESERVATION_LIST", reservation_list);
        request.setAttribute("SERVICE_LIST", service_list);
        request.setAttribute("PATIENT_LIST", patient_list);
        
        RequestDispatcher rd = request.getRequestDispatcher("Reservation_inner.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
