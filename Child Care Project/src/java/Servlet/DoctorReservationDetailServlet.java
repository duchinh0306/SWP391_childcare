/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet;

import Model.*;
import dal.DAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 *
 * @author duchi
 */
@WebServlet(name = "DoctorReservationDetailServlet", urlPatterns = {"/DoctorReservationDetail"})
public class DoctorReservationDetailServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAO dao = new DAO();
        HttpSession session = request.getSession();

        //lay account
        Account account = (Account) session.getAttribute("ACCOUNT");

        //lay ra reservation id
        String reservationId = request.getParameter("reservationId");
        if (reservationId == null) {
            response.sendRedirect("DoctorReservation");
        } else {

            //change
            int id = Integer.parseInt(reservationId);

            //lay ra reservation 
            Reservation2 reservation = dao.get_reservation_by_id(id);

            if (reservation.getStaffId() != account.getAccountId()) {
                response.sendRedirect("DoctorReservation");
            } else {

                //lay ra reservation list
                //ArrayList<Reservation2> reservation_list = dao.get_reservation_list();

                //lay ra danh sach reservation detial theo id
                ArrayList<ReservationDetail2> reservation_detail_list = dao.get_reservation_detail_by_id(id);

                //lay ra thong tin chidlren
                Children children = dao.get_children_by_id(reservation.getChildrenId());

                //lay ra thong tin bac si
                Account doctor = dao.get_staff_by_id(reservation.getStaffId());

                //lay ra service list
                ArrayList<Services> service_list = dao.get_service_list();
                
                //lay ra patient
                Account patient = dao.get_patient_by_id(reservation.getPatientId());
                
                request.setAttribute("PATIENT", patient);
                request.setAttribute("SERVICE_LIST", service_list);
                request.setAttribute("DOCTOR", doctor);
                request.setAttribute("RESERVATION", reservation);
                request.setAttribute("CHILDREN", children);
                request.setAttribute("RESERVATION_DETAIL", reservation_detail_list);
                RequestDispatcher rd = request.getRequestDispatcher("DoctorReservationDetail_inner.jsp");
                rd.forward(request, response);
            }

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
