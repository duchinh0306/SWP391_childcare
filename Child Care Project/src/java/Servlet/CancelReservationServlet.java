/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet;

import Model.Account;
import Model.Reservation2;
import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 *
 * @author duchi
 */
@WebServlet(name = "CancelReservationServlet", urlPatterns = {"/CancelReservation"})
public class CancelReservationServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CancelReservationServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CancelReservationServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("Home");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAO dao = new DAO();
        HttpSession session = request.getSession();

        String type = request.getParameter("txtType");
        if (type.equals("DO IT")) {
            //lay ra account id
            Account account = (Account) session.getAttribute("ACCOUNT");
            int id = account.getAccountId();

            //lay ra id
            int reservationId = Integer.parseInt(request.getParameter("txtRID"));

            //check
            boolean check = false;
            ArrayList<Reservation2> reservation_list = dao.get_reservation_by_patientId(id);
            for (Reservation2 reservation : reservation_list) {
                if (reservation.getPatientId() == id) {
                    check = true;
                    break;
                }
            }
            
            if(check){
                dao.update_reservation_status(4,reservationId);
            }
            
            response.sendRedirect("MyReservationDetail?reservationId="+reservationId);
            
        } else {
            response.sendRedirect("MyReservation");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
