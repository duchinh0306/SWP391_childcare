/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet;

import dal.DAO;
import Model.*;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 *
 * @author duchi
 */
@WebServlet(name = "MyReservationDetailServlet", urlPatterns = {"/MyReservationDetail"})
public class MyReservationDetailServlet extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MyReservationDetailServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MyReservationDetailServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAO dao = new DAO();
        HttpSession session = request.getSession();

        //lay ra account tu session
        Account account = (Account) session.getAttribute("ACCOUNT");
        
        if (account != null && account.getRoleId() == 1) {

            //lay ra reservation id
            String id = request.getParameter("reservationId");
            
            if (id != null) {

                //lay ra reservation theo id
                Reservation2 reservation = dao.get_reservation_by_id(Integer.parseInt(id));

                //lay ra reservation list
                ArrayList<Reservation2> reservation_list = dao.get_reservation_list();

                //lay ra danh sach reservation detial theo id
                ArrayList<ReservationDetail2> reservation_detail_list = dao.get_reservation_detail_by_id(Integer.parseInt(id));

                //lay ra thong tin chidlren
                Children children = dao.get_children_by_id(reservation.getChildrenId());

                //lay ra thong tin bac si
                Account doctor = dao.get_staff_by_id(reservation.getStaffId());

                //lay ra service list
                ArrayList<Services> service_list = dao.get_service_list();
                
                request.setAttribute("ACCOUNT", account);
                request.setAttribute("SERVICE_LIST", service_list);
                request.setAttribute("DOCTOR", doctor);
                request.setAttribute("RESERVATION", reservation);
                request.setAttribute("CHILDREN", children);
                request.setAttribute("RESERVATION_DETAIL", reservation_detail_list);
                
                boolean check = false;
                for (Reservation2 x : reservation_list) {
                    if (x.getReservationId() == reservation.getReservationId()) {
                        if (x.getPatientId() == account.getAccountId()) {
                            check = true;
                        }
                    }
                }
                
                if (check) {
                    RequestDispatcher rd = request.getRequestDispatcher("MyReservationDetail_inner.jsp");
                    rd.forward(request, response);
                } else {
                    response.sendRedirect("MyReservation");
                }
                
            } else {
                response.sendRedirect("MyReservation");
            }
            
        } else {
            request.setAttribute("LOGIN_VALID", "Please Login First.");
            RequestDispatcher rd = request.getRequestDispatcher("Login_inner.jsp");
            rd.forward(request, response);
        }
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
