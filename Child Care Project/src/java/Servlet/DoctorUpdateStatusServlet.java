/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet;

import Model.*;
import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author duchi
 */
@WebServlet(name = "DoctorUpdateStatusServlet", urlPatterns = {"/DoctorUpdateStatus"})
public class DoctorUpdateStatusServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAO dao = new DAO();
        HttpSession session = request.getSession();

        //get account
        Account account = (Account) session.getAttribute("ACCOUNT");

        //get parameter
        String reservationId = request.getParameter("txtID");
        String type = request.getParameter("txtType");
        //check
        if (reservationId != null) {
            Reservation2 reservation = dao.get_reservation_by_id(Integer.parseInt(reservationId));
            if (reservation.getStaffId() == account.getAccountId() && reservation.getStatusId() == 2) {
                if (type.equals("completed")) {
                    dao.update_reservation_status(3, reservation.getReservationId());
                } else if (type.equals("noshow")) {
                    dao.update_reservation_status(6, reservation.getReservationId());
                }
            }
        }

        response.sendRedirect("DoctorReservationDetail?reservationId=" + reservationId);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
