/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet;

import Model.Account;
import Model.Services;
import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author BlackZ36
 */
@WebServlet(name = "ReservationCompletionServlet", urlPatterns = {"/ReservationCompletion"})
public class ReservationCompletionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ReservationCompletionServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ReservationCompletionServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("Home");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAO dao = new DAO();
        HttpSession session = request.getSession();

        //lay ra account
        Account account = (Account) session.getAttribute("ACCOUNT");

        //lay ra service list tu session
        ArrayList<Services> service_list = (ArrayList<Services>) session.getAttribute("CHOSEN_SERVICE_LIST");

        //lay ra parameter
        int slotId = Integer.parseInt(request.getParameter("txtSlot"));
        int patientId = account.getAccountId();
        int staffId = Integer.parseInt(request.getParameter("txtDoctor"));
        int childId = Integer.parseInt(request.getParameter("txtChildren"));
        String dateString = request.getParameter("txtDate");

        //tinh total
        double total = 0;
        int add = 0;
        boolean detail = false;
        if (service_list == null || service_list.isEmpty()) {
            response.sendRedirect("ReservationDetail");
        } else {
            for (Services service : service_list) {
                total += service.getPrice() - (service.getPrice() * (service.getDiscount() / 100));
            }
        }

        try {
            //chuyen date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date parsedDate = sdf.parse(dateString);
            java.sql.Date sqlDate = new java.sql.Date(parsedDate.getTime());

            //add reservation
            add = dao.add_reservation(slotId, patientId, childId, staffId, sqlDate, total);

            //add reservation detail
            if (add != 0) {
                for (Services service : service_list) {
                    double price = service.getPrice() - (service.getPrice() * (service.getDiscount() / 100));
                    detail = dao.add_reservation_detail(add, service.getServiceId(), price);
                }
            }

            String realPath = getServletContext().getRealPath("/");
            // Tạo đường dẫn tuyệt đối đến tệp tin template
            String templateFilePath = realPath + "/email_reservation_template.html";
            // Đọc nội dung từ tệp tin template
            StringBuilder contentBuilder = new StringBuilder();
            try ( InputStream inputStream = new FileInputStream(templateFilePath);  InputStreamReader inputStreamReader = new InputStreamReader(inputStream);  BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    contentBuilder.append(line);
                }
            }
            //send email
            String slot = "";
            String emailTemplate = contentBuilder.toString();
            switch (slotId) {
                case 1:
                    slot = "7:30 - 9:30 (AM)";
                    break;
                case 2:
                    slot = "9:30 - 11:30 (AM)";
                    break;
                case 3:
                    slot = "14:30 - 16:30 (PM)";
                    break;
                case 4:
                    slot = "16:30 - 18:30 (PM)";
                    break;
                default:
                    break;
            }

            dao.Send_Reservation_Email(account.getEmail(), account.getName(), emailTemplate, childId, dateString, slot);
            session.removeAttribute("CHOSEN_SERVICE_LIST");
            response.sendRedirect("MyReservation?NOTIBOX=true&NOTICONTENT=Reservation Booked Successfully");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
