/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Servlet;

import Model.Account;
import Model.Services;
import dal.DAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author duchi
 */
@WebServlet(name = "CheckAvailableServlet", urlPatterns = {"/CheckAvailable"})
public class CheckAvailableServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CheckAvailableServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CheckAvailableServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String type = request.getParameter("btnType");
        DAO dao = new DAO();
        HttpSession session = request.getSession();

        //lay ra account
        Account account = (Account) session.getAttribute("ACCOUNT");

        if (type == null) {
            response.sendRedirect("ReservationContact");
        } else {
            if (type.equals("check")) {

                String childExist = request.getParameter("txtChildren");
                if (childExist == null) {
                    response.sendRedirect("ReservationContact?childrenExist=denied");
                }

                String dateString = request.getParameter("txtDate");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    java.util.Date parsedDate = sdf.parse(dateString);
                    java.sql.Date selectedDate = new java.sql.Date(parsedDate.getTime());
                    java.sql.Date currentDate = new java.sql.Date(System.currentTimeMillis());
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(currentDate);
                    calendar.add(Calendar.MONTH, 1);
                    java.util.Date oneMonthLater = calendar.getTime();

                    if (selectedDate.compareTo(currentDate) <= 0 || selectedDate.compareTo(oneMonthLater) > 0) {
                        int slot = Integer.parseInt(request.getParameter("txtSlot"));
                        int doctorId = Integer.parseInt(request.getParameter("txtDoctor"));
                        int childId = Integer.parseInt(request.getParameter("txtChildren"));

                        response.sendRedirect("ReservationContact?Date=old&doctorId=" + doctorId + "&slotId=" + slot + "&DateSelect=" + selectedDate + "&ChildrenId=" + childId);

                    } else {
                        int slot = Integer.parseInt(request.getParameter("txtSlot"));
                        int doctorId = Integer.parseInt(request.getParameter("txtDoctor"));
                        int childId = Integer.parseInt(request.getParameter("txtChildren"));

                        boolean available = dao.check_slot_available(doctorId, slot, selectedDate);

                        if (available) {
                            //response.getWriter().write(dateString);
                            response.sendRedirect("ReservationContact?Date=ok&doctorId=" + doctorId + "&slotId=" + slot + "&DateSelect=" + selectedDate + "&ChildrenId=" + childId);
                             
                       } else {
                            response.sendRedirect("ReservationContact?Date=unavailable&doctorId=" + doctorId + "&slotId=" + slot + "&DateSelect=" + selectedDate + "&ChildrenId=" + childId);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (type.equals("add")) {
                //kiem tra child
                String childExist = request.getParameter("txtChildren");
                if (childExist == null) {
                    response.sendRedirect("ReservationContact?childrenExist=denied");
                }
                
                
                
                //lay ra account tu session
                Account patient = (Account) session.getAttribute("ACCOUNT");

                //lay ra service list tu session
                ArrayList<Services> service_list = (ArrayList<Services>) session.getAttribute("CHOSEN_SERVICE_LIST");
                
                //kiem tra service cart
                if(service_list == null || service_list.isEmpty()){
                    response.sendRedirect("ReservationDetail");
                }
                
                //tinh total
                double total = 0;
                for(Services service : service_list) {
                    total += service.getPrice() - (service.getPrice() * (service.getDiscount() / 100));
                }

                //lay ra parameter
                int patientId = patient.getAccountId();
                int slotId = Integer.parseInt(request.getParameter("txtSlot"));
                int staffId = Integer.parseInt(request.getParameter("txtDoctor"));
                String dateString = request.getParameter("txtDate");
                int childId = Integer.parseInt(request.getParameter("txtChildren"));

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    java.util.Date parsedDate = sdf.parse(dateString);
                    java.sql.Date sqlDate = new java.sql.Date(parsedDate.getTime());

                    //them vao bang Reservation
                    String payment = request.getParameter("txtPayment");
                    int add = 1;
                    if (payment.equals("cash")) {

                    } else if (payment.equals("banking")) {
                        //add = dao.add_reservation_bank(slotId, patientId, childId, staffId, sqlDate, total);
                        //response.getWriter().write("cash");
                        response.sendRedirect("ReservationContact");
                        return;
                        //gui di email huong dan thanh toan
                    }



                    if (add != 0 && true) {
                        //response.getWriter().write("id: " + add);

                        //total
                        double price = 0;
                        for (Services service : service_list) {
                            price += service.getPrice() - (service.getPrice() * (service.getDiscount() / 100));
                        }

                        String slot = "";
                        switch (slotId) {
                            case 1:
                                slot = "7:30 - 9:30 (AM)";
                                break;
                            case 2:
                                slot = "9:30 - 11:30 (AM)";
                                break;
                            case 3:
                                slot = "14:30 - 16:30 (PM)";
                                break;
                            case 4:
                                slot = "16:30 - 18:30 (PM)";
                                break;
                            default:
                                break;
                        }
                        
                        
                        
                        //set attribute
                        request.setAttribute("CATEGORY_LIST", dao.get_service_category_list());
                        request.setAttribute("CHOSEN_SERVICE_LIST", service_list);
                        request.setAttribute("DOCTOR", dao.get_staff_by_id(staffId));
                        request.setAttribute("CHILDREN", dao.get_children_by_id(childId));
                        request.setAttribute("DATE", dateString);
                        request.setAttribute("SLOTID", slotId);
                        request.setAttribute("SLOT", slot);
                        request.setAttribute("total", price);

                        RequestDispatcher rd = request.getRequestDispatcher("ReservationCompletion_inner.jsp");
                        rd.forward(request, response);
                    } else {
                        response.getWriter().write("patientid:" + patientId + "- slotid:" + slotId + " - childId:" + childId + " - staffId:" + staffId + " - Date:" + dateString + " - total: " + total + " - payment: " + payment);
                        //throw new Exception();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //response.getWriter().write("patientid:" + patientId + "- slotid:" + slotId + " - childId:" + childId + " - staffId:" + staffId + " - Date:" + dateString + " - total: " + total);
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
