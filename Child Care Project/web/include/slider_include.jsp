<%-- 
    Document   : slider_include
    Created on : Oct 5, 2023, 2:10:20 PM
    Author     : BlackZ36
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <section id="hero">
            <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">

                <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

                <div class="carousel-inner" role="listbox">

                    <!-- Slide 1 -->
                    <div class="carousel-item active" style="background-image: url(assets/img/slide/slide-1.jpg)">
                        <div class="container">
                            <h2>Welcome to <span>Medicio</span></h2>
                            <p>Welcome to Medico Children Care, where we prioritize the well-being of your little ones. With a dedicated team of healthcare professionals and a user-friendly platform, we provide comprehensive pediatric care, ensuring your child's health and happiness is our top priority.</p>
                            <a href="#about" class="btn-get-started scrollto">Read More</a>
                        </div>
                    </div>

                    <!-- Slide 2 -->
                    <div class="carousel-item" style="background-image: url(assets/img/slide/slide-2.jpg)">
                        <div class="container">
                            <h2>Discover Health Harmony at Medicio</h2>
                            <p>At Medico Children Care, we specialize in nurturing smiles and ensuring the best possible healthcare experience for your children. Our tailored services and expert medical guidance create a safe haven for your little ones, making their journey to wellness a seamless and caring one.</p>
                            <a href="#about" class="btn-get-started scrollto">Read More</a>
                        </div>
                    </div>

                    <!-- Slide 3 -->
                    <div class="carousel-item" style="background-image: url(assets/img/slide/slide-3.jpg)">
                        <div class="container">
                            <h2>Embark on Your Wellness Journey with Medicio</h2>
                            <p>Medico Children Care: Where Compassionate Care Meets Innovation. With a blend of cutting-edge medical expertise and heartfelt compassion, we're here to nurture your child's health, one smile at a time. Trust us to provide the highest quality pediatric care, ensuring your little ones grow up happy and healthy.</p>
                            <a href="#about" class="btn-get-started scrollto">Read More</a>
                        </div>
                    </div>

                </div>

                <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
                </a>

                <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
                    <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
                </a>

            </div>
        </section>
    </body>
</html>
