<%-- 
    Document   : reservation_completion_include
    Created on : Oct 30, 2023, 2:59:45 PM
    Author     : BlackZ36
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Payment</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link rel="stylesheet" href="payment.css">
        <style>
            .payment-form{
                padding-bottom: 50px;
                font-family: 'Montserrat', sans-serif;
            }

            .payment-form.dark{
                background-color: #f6f6f6;
            }

            .payment-form .content{
                box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);
                background-color: white;
            }

            .payment-form .block-heading{
                padding-top: 50px;
                margin-bottom: 40px;
                text-align: center;
            }

            .payment-form .block-heading p{
                text-align: center;
                max-width: 420px;
                margin: auto;
                opacity:0.7;
            }

            .payment-form.dark .block-heading p{
                opacity:0.8;
            }

            .payment-form .block-heading h1,
            .payment-form .block-heading h2,
            .payment-form .block-heading h3 {
                margin-bottom:1.2rem;
                color: #3b99e0;
            }

            .payment-form form{
                border-top: 2px solid #5ea4f3;
                box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);
                background-color: #ffffff;
                padding: 0;
                max-width: 600px;
                margin: auto;
            }

            .payment-form .title{
                font-size: 1em;
                border-bottom: 1px solid rgba(0,0,0,0.1);
                margin-bottom: 0.8em;
                font-weight: 600;
                padding-bottom: 8px;
            }

            .payment-form .products{
                background-color: #f7fbff;
                padding: 25px;
            }

            .payment-form .products .item{
                margin-bottom:1em;
            }

            .payment-form .products .item-name{
                font-weight:600;
                font-size: 0.9em;
            }

            .payment-form .products .item-description{
                font-size:0.8em;
                opacity:0.6;
            }

            .payment-form .products .item p{
                margin-bottom:0.2em;
            }

            .payment-form .products .price{
                float: right;
                font-weight: 600;
                font-size: 0.9em;
            }

            .payment-form .products .total{
                border-top: 1px solid rgba(0, 0, 0, 0.1);
                margin-top: 10px;
                padding-top: 19px;
                font-weight: 600;
                line-height: 1;
            }

            .payment-form .card-details{
                padding: 25px 25px 15px;
            }

            .payment-form .card-details label{
                font-size: 12px;
                font-weight: 600;
                margin-bottom: 15px;
                color: #79818a;
                text-transform: uppercase;
            }

            .payment-form .card-details button{
                margin-top: 0.6em;
                padding:12px 0;
                font-weight: 600;
            }

            .payment-form .date-separator{
                margin-left: 10px;
                margin-right: 10px;
                margin-top: 5px;
            }

            @media (min-width: 576px) {
                .payment-form .title {
                    font-size: 1.2em;
                }

                .payment-form .products {
                    padding: 40px;
                }

                .payment-form .products .item-name {
                    font-size: 1em;
                }

                .payment-form .products .price {
                    font-size: 1em;
                }

                .payment-form .card-details {
                    padding: 40px 40px 30px;
                }

                .payment-form .card-details button {
                    margin-top: 2em;
                }
            }
        </style>
    </head>
    <body>
        <main class="page payment-page">
            <section class="payment-form dark" style="padding: 0px;">
                <div class="container">
                    <div class="block-heading">
                        <h2>Reservation Completion</h2>
                        <p>Please Re-Check You Final Reservation Detail.</p>
                    </div>
                    <form method="POST" action="ReservationCompletion"> 
                        <div class="products">
                            <h3 class="title">Service(s)</h3>


                            <c:forEach var="service" items="${CHOSEN_SERVICE_LIST}">
                                <div class="item">
                                    <div class="row">
                                        <div class="form-group col-sm-3">
                                            <img src="assets/img/service/${service.image}" width="80" height="80" alt="alt"/>
                                        </div>
                                        <div class="form-group col-sm-6">    
                                            <p class="item-name">${service.name}</p>
                                            <c:forEach var="category" items="${CATEGORY_LIST}">
                                                <c:if test="${category.categoryId eq service.categoryId}">
                                                    <p class="item-description">category: ${category.name} </p>
                                                </c:if>
                                            </c:forEach>
                                        </div>

                                        <div class="form-group col-sm-3">
                                            <p class="item-name">${service.price} $</p>
                                        </div>

                                    </div>
                                </div>

                            </c:forEach>

                            <div class="total">Total<span class="price">$${total}</span></div>
                        </div>


                        <div class="card-details">
                            <h3 class="title">Doctor In Charge</h3>
                            <div class="row">

                                <div class="form-group col-sm-5">
                                    <img src="assets/img/user/${DOCTOR.username}/${DOCTOR.avatar}" style="border-radius: 5px;" width="220" alt="alt"/>
                                </div>

                                <div class="form-group col-sm-7">
                                    <label for="card-holder">Doctor:</label>
                                    <input id="card-holder" type="text" class="form-control" value="${DOCTOR.name}" aria-label="Card Holder" aria-describedby="basic-addon1" readonly>
                                    <label class="mt-3" for="card-holder">Contact:</label>
                                    <input id="card-holder" type="text" class="form-control" value="${DOCTOR.email}" aria-label="Card Holder" aria-describedby="basic-addon1" readonly>
                                    <input id="card-holder" type="text" class="form-control mt-3" value="${DOCTOR.phone}" aria-label="Card Holder" aria-describedby="basic-addon1" readonly>
                                    <input type="hidden" name="txtDoctor" value="${DOCTOR.accountId}" >
                                </div>

                            </div>
                        </div>

                        <div class="card-details">
                            <h3 class="title">Your Children's Information</h3>
                            <div class="row">
                                <div class="form-group col-sm-7">
                                    <label for="card-holder">Children Name</label>
                                    <input id="card-holder" type="text" class="form-control" value="${CHILDREN.name}" aria-label="Card Holder" aria-describedby="basic-addon1" readonly>
                                </div>
                                <div class="form-group col-sm-3">
                                    <input id="card-holder" type="hidden" class="form-control" name="txtChildren" value="${CHILDREN.childrenId}" aria-label="Card Holder" aria-describedby="basic-addon1" readonly>
                                </div>
                                <div class="form-group col-sm-7">
                                    <label for="cvc">Date Of Birth</label>
                                    <input id="cvc" type="text" class="form-control" value="${CHILDREN.dob}" aria-label="Card Holder" aria-describedby="basic-addon1" readonly>
                                </div>
                                <div class="form-group col-sm-5">
                                    <label for="">Gender</label>
                                    <div class="input-group expiration-date">
                                        <c:if test="${CHILDREN.gender == true}">
                                            <input id="card-number" type="text" class="form-control" value="Male" aria-label="Card Holder" aria-describedby="basic-addon1" readonly>
                                        </c:if>
                                        <c:if test="${CHILDREN.gender == false}">
                                            <input id="card-number" type="text" class="form-control" value="Female" aria-label="Card Holder" aria-describedby="basic-addon1" readonly>
                                        </c:if>
                                    </div>
                                </div>


                            </div>
                        </div>


                        <div class="card-details">
                            <h3 class="title">Check Up Time</h3>
                            <div class="row">
                                <div class="form-group col-sm-7">
                                    <label for="card-holder">Date</label>
                                    <input id="card-holder" type="text" class="form-control" name="txtDate" value="${DATE}" aria-label="Card Holder" aria-describedby="basic-addon1" readonly>
                                </div>

                                <div class="form-group col-sm-4">
                                    <input id="card-number" type="hidden" class="form-control" name="" value=" aria-label="Card Holder" aria-describedby="basic-addon1" readonly>
                                </div>

                                <div class="form-group col-sm-7">
                                    <label for="cvc">Slot Time</label>
                                    <input id="cvc" type="text" class="form-control" value="${SLOT}" aria-label="Card Holder" aria-describedby="basic-addon1" readonly>
                                </div>
                                <div class="form-group col-sm-4">
                                    <label for="card-number">Slot Number</label>
                                    <input id="card-number" type="text" class="form-control" name="txtSlot" value="${SLOTID}" aria-label="Card Holder" aria-describedby="basic-addon1" readonly>
                                </div>

                            </div>
                        </div>
                        <div class="form-group col-sm-12" style="padding-bottom: 30px">
                            <button type="submit" class="btn btn-primary btn-block">Checkout Reservation</button>
                        </div>

                    </form>
                </div>
            </section>
        </main>
    </body>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>
