<%-- 
    Document   : my_reservation_detail_inlucde
    Created on : Oct 28, 2023, 12:30:27 AM
    Author     : duchi
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            body{
                margin-top:20px;
                background:#eee;
            }
            /* My Account */
            .payments-item img.mr-3 {
                width: 47px;
            }
            .order-list .btn {
                border-radius: 2px;
                min-width: 121px;
                font-size: 13px;
                padding: 7px 0 7px 0;
            }
            .osahan-account-page-left .nav-link {
                padding: 18px 20px;
                border: none;
                font-weight: 600;
                color: #535665;
            }
            .osahan-account-page-left .nav-link i {
                width: 28px;
                height: 28px;
                background: #535665;
                display: inline-block;
                text-align: center;
                line-height: 29px;
                font-size: 15px;
                border-radius: 50px;
                margin: 0 7px 0 0px;
                color: #fff;
            }
            .osahan-account-page-left .nav-link.active {
                background: #f3f7f8;
                color: #282c3f !important;
            }
            .osahan-account-page-left .nav-link.active i {
                background: #282c3f !important;
            }
            .osahan-user-media img {
                width: 90px;
            }
            .card offer-card h5.card-title {
                border: 2px dotted #000;
            }
            .card.offer-card h5 {
                border: 1px dotted #daceb7;
                display: inline-table;
                color: #17a2b8;
                margin: 0 0 19px 0;
                font-size: 15px;
                padding: 6px 10px 6px 6px;
                border-radius: 2px;
                background: #fffae6;
                position: relative;
            }
            .card.offer-card h5 img {
                height: 22px;
                object-fit: cover;
                width: 22px;
                margin: 0 8px 0 0;
                border-radius: 2px;
            }
            .card.offer-card h5:after {
                border-left: 4px solid transparent;
                border-right: 4px solid transparent;
                border-bottom: 4px solid #daceb7;
                content: "";
                left: 30px;
                position: absolute;
                bottom: 0;
            }
            .card.offer-card h5:before {
                border-left: 4px solid transparent;
                border-right: 4px solid transparent;
                border-top: 4px solid #daceb7;
                content: "";
                left: 30px;
                position: absolute;
                top: 0;
            }
            .payments-item .media {
                align-items: center;
            }
            .payments-item .media img {
                margin: 0 40px 0 11px !important;
            }
            .reviews-members .media .mr-3 {
                width: 56px;
                height: 56px;
                object-fit: cover;
            }
            .order-list img.mr-4 {
                width: 70px;
                height: 70px;
                object-fit: cover;
                box-shadow: 0 .125rem .25rem rgba(0, 0, 0, .075)!important;
                border-radius: 2px;
            }
            .osahan-cart-item p.text-gray.float-right {
                margin: 3px 0 0 0;
                font-size: 12px;
            }
            .osahan-cart-item .food-item {
                vertical-align: bottom;
            }

            .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
                color: #000000;
            }

            .shadow-sm {
                box-shadow: 0 .125rem .25rem rgba(0,0,0,.075)!important;
            }

            .rounded-pill {
                border-radius: 50rem!important;
            }
            a:hover{
                text-decoration:none;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="osahan-account-page-left shadow-sm bg-white h-100">
                        <div class="border-bottom p-4" style="background-color: #ecf8f9;">
                            <div class="osahan-user text-center">
                                <div class="osahan-user-media">
                                    <h5 class="mb-2">Doctor In Charge</h5>
                                    <img class="mb-3 rounded-4 shadow-sm mt-3" src="assets/img/user/${DOCTOR.username}/${DOCTOR.avatar}" alt="doctor avatar">
                                    <div class="osahan-user-media-body">
                                        <h6 class="mb-2">Name: ${DOCTOR.name}</h6>
                                        <p class="mb-1">Phone: ${DOCTOR.phone}</p>
                                        <p>Email: ${DOCTOR.email}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-tabs flex-column border-0 pl-4 pb-4" id="myTab" role="tablist">
                            <div class="border-bottom p-4">
                                <div class="osahan-user text-center">
                                    <div class="osahan-user-media">
                                        <h5 class="mb-2">Your Children Information</h5>
                                        <div class="osahan-user-media-body">
                                            <h6 class="mb-2">Name: ${CHILDREN.name}</h6>
                                            <p class="mb-1">
                                                <c:if test="${CHILDREN.gender == true}">
                                                    Gender: Boy
                                                </c:if>
                                                <c:if test="${CHILDREN.gender == false}">
                                                    Gender: Girl
                                                </c:if>
                                            </p>
                                            <p class="mb-1" > Date Of Birth: ${CHILDREN.dob}</p>
                                            <p>
                                                <c:if test="${CHILDREN.relation == 1}">
                                                    Relationship: Son
                                                </c:if>
                                                <c:if test="${CHILDREN.relation == 2}">
                                                    Relationship: Daughter
                                                </c:if>
                                                <c:if test="${CHILDREN.relation == 3}">
                                                    Relationship: Other
                                                </c:if>
                                            </p>
                                            <p class="button btn-primary"><a class="text-primary mr-3" href="MyChildren"><i class="icofont-ui-edit"></i>EDIT</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ul>
                        <ul class="nav nav-tabs flex-column border-0 pl-4 pb-4" id="myTab" role="tablist">
                            <div class="p-4">
                                <div class="osahan-user text-center">
                                    <div class="osahan-user-media">
                                        <h5 class="mb-2">Your Information</h5>
                                        <img class="mb-3 rounded-4 shadow-sm mt-3" src="assets/img/user/${ACCOUNT.username}/${ACCOUNT.avatar}" alt="customer avatar">
                                        <div class="osahan-user-media-body">
                                            <h6 class="mb-2">Name: ${ACCOUNT.name}</h6>
                                            <p class="mb-1">
                                                <c:if test="${ACCOUNT.gender == true}">
                                                    Gender: Male
                                                </c:if>
                                                <c:if test="${ACCOUNT.gender == false}">
                                                    Gender: Female
                                                </c:if>
                                            </p>
                                            <p class="mb-1">Phone: ${ACCOUNT.phone}</p>
                                            <p>Email: ${ACCOUNT.email}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="osahan-account-page-right shadow-sm bg-white p-4 h-100">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade  active show" id="favourites" role="tabpanel" aria-labelledby="favourites-tab">

                                <div class="row">

                                    <h4 class="font-weight-bold mt-4 mb-5 text-center">My Reservation Information</h4>
                                    <div class="col-md-12 text-left load-more border-bottom mb-5">
                                        <div class="mb-5" >
                                            <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                <i class="fa-solid fa-list-check" style="margin-right: 10px;"></i>
                                                <span class="mr-3" style="width: 200px;">Reservation ID:</span>
                                                <input style="width: 300px; background-color: #f2f2f2;" class="form-control" value="${RESERVATION.reservationId}" readonly/>
                                            </div> 
                                            <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                <i class="fa-regular fa-calendar-days" style="margin-right: 10px;"></i>
                                                <span class="mr-3" style="width: 200px;">Booked Date:</span>
                                                <input style="width: 300px; background-color: #f2f2f2;" class="form-control" value="${RESERVATION.bookDate}" readonly/>
                                            </div>
                                            <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                <i class="fa-regular fa-calendar-days" style="margin-right: 10px;"></i>
                                                <span class="mr-3" style="width: 200px; font-weight: bold;">Check Up Date:</span>
                                                <input style="width: 300px; background-color: #f2f2f2; font-weight: bold;" class="form-control" value="${RESERVATION.date}" readonly/>
                                            </div>
                                            <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                <i class="fa-regular fa-calendar-days" style="margin-right: 10px;"></i>
                                                <span class="mr-3" style="width: 200px; font-weight: bold;">Slot Number:</span>
                                                <input style="width: 300px; background-color: #f2f2f2; font-weight: bold;" class="form-control" value="${RESERVATION.slotId}" readonly/>
                                            </div>
                                            <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                <i class="fa-regular fa-clock" style="margin-right: 10px;"></i>
                                                <span class="mr-3" style="width: 200px; font-weight: bold;">Time:</span>
                                                <c:if test="${RESERVATION.slotId == 1}">
                                                    <input style="width: 300px; background-color: #f2f2f2; font-weight: bold;" class="form-control" value="7:30 - 9:30 (AM)" readonly/>
                                                </c:if>
                                                <c:if test="${RESERVATION.slotId == 2}">
                                                    <input style="width: 300px; background-color: #f2f2f2; font-weight: bold;" class="form-control" value="9:30 - 11:30 (AM)" readonly/>
                                                </c:if>
                                                <c:if test="${RESERVATION.slotId == 3}">
                                                    <input style="width: 300px; background-color: #f2f2f2; font-weight: bold;" class="form-control" value="14:30 - 16:30 (PM)" readonly/>
                                                </c:if>
                                                <c:if test="${RESERVATION.slotId == 4}">
                                                    <input style="width: 300px; background-color: #f2f2f2; font-weight: bold;" class="form-control" value="16:30 - 18:30 (PM)" readonly/>
                                                </c:if>

                                            </div>
                                            <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                <i class="fa-solid fa-file-invoice-dollar" style="margin-right: 10px;"></i>
                                                <span class="mr-3" style="width: 200px;">Total:</span>
                                                <input style="width: 300px; background-color: #f2f2f2;" class="form-control" value="$${RESERVATION.total}" readonly/>
                                            </div>
                                            <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                <i class="fa-solid fa-hand-holding-medical" style="margin-right: 10px;"></i>
                                                <span class="mr-3" style="width: 200px;">Status:</span>
                                                <c:choose>
                                                    <c:when test="${RESERVATION.statusId eq 1}">
                                                        <input style="width: 300px; background-color: #f2f2f2;" class="form-control" value="Pending Confirmation" readonly/>
                                                    </c:when>
                                                    <c:when test="${RESERVATION.statusId eq 2}">
                                                        <input style="width: 300px; background-color: #f2f2f2;" class="form-control" value="Confirmed" readonly/>
                                                    </c:when>
                                                    <c:when test="${RESERVATION.statusId eq 3}">
                                                        <input style="width: 300px; background-color: #f2f2f2;" class="form-control" value="Completed" readonly/>
                                                    </c:when>
                                                    <c:when test="${RESERVATION.statusId eq 4}">
                                                        <input style="width: 300px; background-color: #f2f2f2;" class="form-control" value="Cancelled" readonly/>
                                                    </c:when>
                                                    <c:when test="${RESERVATION.statusId eq 5}">
                                                        <input style="width: 300px; background-color: #f2f2f2;" class="form-control" value="Rescheduled" readonly/>
                                                    </c:when>
                                                    <c:when test="${RESERVATION.statusId eq 6}">
                                                        <input style="width: 300px; background-color: #f2f2f2;" class="form-control" value="No Show" readonly/>
                                                    </c:when>
                                                    <c:when test="${RESERVATION.statusId eq 7}">
                                                        <input style="width: 300px; background-color: #f2f2f2;" class="form-control" value="Pending Transaction" readonly/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <input style="width: 300px; background-color: #f2f2f2;" class="form-control" value="Unknown Status" readonly/>
                                                    </c:otherwise>
                                                </c:choose>

                                            </div>
                                        </div>
                                        <!--button zone start-->
                                        <div class="col-md-12 text-center load-more mb-4" style="">
                                            <c:if test="${RESERVATION.statusId == 1 || RESERVATION.statusId == 7}">
                                                <button type="button" class="deleteBtn" name="txtType" style="font-weight: thin; padding: 10px; background-color: #a6a6a6; border: none; border-radius: 5px; color: white" data-reservationid="${RESERVATION.reservationId}">
                                                    <i class="fas fa-trash"  style="font-size: 20px;"></i>CANCEL
                                                </button>
                                            </c:if>
                                            <c:if test="${RESERVATION.statusId == 7}">
                                                <button class="btn btn-primary" type="button" style="font-size: 14px; background-color: #5adbb5; border: none; width: 110px; height: 50px;">
                                                    FINISH PAYMENT
                                                </button>
                                            </c:if>
                                        </div>
                                        <!--button zone end-->
                                    </div>



                                    <h4 class="font-weight-bold mt-0 mb-5 text-center">Selected Medical Service(s)</h4>
                                    <c:forEach  var="detail" items="${RESERVATION_DETAIL}">
                                        <c:forEach var="service" items="${SERVICE_LIST}">
                                            <c:if test="${service.serviceId eq detail.serviceId}">
                                                <div class="col-md-4 col-sm-6 mb-4 pb-2">
                                                    <div class="list-card bg-white h-100 rounded overflow-hidden position-relative shadow-sm">
                                                        <div class="list-card-image">
                                                            <a href="ServiceDetail?serviceID=${service.serviceId}" target="_blank">
                                                                <img src="assets/img/service/${service.image}" class="img-fluid item-img w-100">
                                                            </a>
                                                        </div>
                                                        <div class="p-3 position-relative">
                                                            <div class="list-card-body">
                                                                <h6 class="mb-1">
                                                                    <a href="ServiceDetail?serviceID=${service.serviceId}" target="_blank" class="text-black">${service.name}</a>
                                                                </h6>
                                                                <p class="text-gray mb-3 time"><span class="bg-light text-dark rounded-sm pl-2 pb-1 pt-1 pr-2"><i class="fa-solid fa-money-bill-wave"></i> ${detail.price}$</span></p>
                                                            </div>
                                                            <c:if test="${RESERVATION.statusId == 3}">
                                                                <div class="list-card-badge">
                                                                    <button class="button btn-primary rounded-2 mt-5" style="background-color: #3fbbc0; color: white; border: none; padding: 5px;">Feedback</button>
                                                                </div>
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:if>
                                        </c:forEach>
                                    </c:forEach>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--remove modal begin-->
        <div id="deleteEmployeeModal" class="modal fade">
            <div class="modal-dialog" style="margin-top: 180px;">
                <div class="modal-content">
                    <form method="post" action="CancelReservation">
                        <div class="modal-header">						
                            <div class="form-group">
                                <label>Cancel Reservation ID?</label>
                                <input type="text" class="form-control" style="width: 40px; margin-top: 10px;" name="txtRID" id="ID" value="" readonly>
                            </div>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">					
                            <p>Are you sure you want to cancel these Reservation?</p>
                            <p class="text-warning"><small>This action cannot be undone.</small></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <input type="submit" name="txtType" class="btn btn-danger" value="DO IT">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--remove modal end-->
        <script>
            document.querySelectorAll('.deleteBtn').forEach(function (button) {
                button.addEventListener('click', function () {
                    var reservationId = this.getAttribute('data-reservationid');
                    // Thiết lập giá trị accountId cho modal

                    //change
                    document.querySelector('#ID').value = reservationId;
                    // Mở modal
                    $('#deleteEmployeeModal').modal('show');
                });
            });

            // Bắt sự kiện click của nút "Cancel" và nút "X"
            document.querySelector('#deleteEmployeeModal .modal-footer .btn-default').addEventListener('click', function () {
                // Đóng modal
                $('#deleteEmployeeModal').modal('hide');
            });

            document.querySelector('#deleteEmployeeModal .modal-header .close').addEventListener('click', function () {
                // Đóng modal
                $('#deleteEmployeeModal').modal('hide');
            });
            document.querySelector('#editEmployeeModal .modal-footer .btn-default').addEventListener('click', function () {
                // Đóng modal
                $('#editEmployeeModal').modal('hide');
            });
            // Bắt sự kiện khi người dùng nhấn phím trên bàn phím
            document.addEventListener('keydown', function (event) {
                // Kiểm tra nếu phím nhấn là phím "Esc" (mã 27)
                if (event.keyCode === 27) {
                    // Đóng modal khi nhấn phím "Esc"
                    $('#deleteEmployeeModal').modal('hide');
                }
            });

        </script>
    </body>
</html>
