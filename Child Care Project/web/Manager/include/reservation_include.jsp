<%-- 
    Document   : patient_manager_include
    Created on : Oct 19, 2023, 2:56:48 AM
    Author     : duchi
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 13px;
                margin: 0;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                min-width: 1000px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #435d7d;
                color: #fff;
                padding: 16px 30px;
                min-width: 100%;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 24px;
            }
            .table-title .btn-group {
                float: right;
            }
            .table-title .btn {
                color: #fff;
                float: right;
                font-size: 13px;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 12px 15px;
                vertical-align: middle;
                text-align: center;
            }
            table.table tr th:first-child {
                width: 30px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 22px;
                margin: 0 5px;
            }


            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }
            /* Custom checkbox */
            .custom-checkbox {
                position: relative;
            }
            .custom-checkbox input[type="checkbox"] {
                opacity: 0;
                position: absolute;
                margin: 5px 0 0 3px;
                z-index: 9;
            }
            .custom-checkbox label:before{
                width: 18px;
                height: 18px;
            }
            .custom-checkbox label:before {
                content: '';
                margin-right: 10px;
                display: inline-block;
                vertical-align: text-top;
                background: white;
                border: 1px solid #bbb;
                border-radius: 2px;
                box-sizing: border-box;
                z-index: 2;
            }
            .custom-checkbox input[type="checkbox"]:checked + label:after {
                content: '';
                position: absolute;
                left: 6px;
                top: 3px;
                width: 6px;
                height: 11px;
                border: solid #000;
                border-width: 0 3px 3px 0;
                transform: inherit;
                z-index: 3;
                transform: rotateZ(45deg);
            }
            .custom-checkbox input[type="checkbox"]:checked + label:before {
                border-color: #03A9F4;
                background: #03A9F4;
            }
            .custom-checkbox input[type="checkbox"]:checked + label:after {
                border-color: #fff;
            }
            .custom-checkbox input[type="checkbox"]:disabled + label:before {
                color: #b8b8b8;
                cursor: auto;
                box-shadow: none;
                background: #ddd;
            }
            /* Modal styles */
            .modal .modal-dialog {
                max-width: 400px;
            }
            .modal .modal-header, .modal .modal-body, .modal .modal-footer {
                padding: 20px 30px;
            }
            .modal .modal-content {
                border-radius: 3px;
                font-size: 14px;
            }
            .modal .modal-footer {
                background: #ecf0f1;
                border-radius: 0 0 3px 3px;
            }
            .modal .modal-title {
                display: inline-block;
            }
            .modal .form-control {
                border-radius: 2px;
                box-shadow: none;
                border-color: #dddddd;
            }
            .modal textarea.form-control {
                resize: vertical;
            }
            .modal .btn {
                border-radius: 2px;
                min-width: 100px;
            }
            .modal form label {
                font-weight: normal;
            }
            .address-cell {
                max-width: 200px; /* Đặt giới hạn chiều rộng của cell */
                overflow: hidden;
                text-overflow: ellipsis; /* Thêm hiệu ứng tròn (ellipsis) cho văn bản dài */
                white-space: nowrap;
            }
        </style>
        <script>
            $(document).ready(function () {
                // Activate tooltip
                $('[data-toggle="tooltip"]').tooltip();

                // Select/Deselect checkboxes
                var checkbox = $('table tbody input[type="checkbox"]');
                $("#selectAll").click(function () {
                    if (this.checked) {
                        checkbox.each(function () {
                            this.checked = true;
                        });
                    } else {
                        checkbox.each(function () {
                            this.checked = false;
                        });
                    }
                });
                checkbox.click(function () {
                    if (!this.checked) {
                        $("#selectAll").prop("checked", false);
                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="container-xxl">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <h2>Manage <b>Reservations</b></h2>
                            </div>


                            <div class="col-sm-4 col-xs-12" style="margin-left: auto">
                                <form action="Search" method="post">
                                    <div class="input-group" >
                                        <input type="text" placeholder="Input Patient keyword" name="txtSearch" class="form-control w-50">
                                        <select name="txtSearchType"  class="form-control">
                                            <option value="1" >ID</option>
                                            <option value="2">Name</option>
                                        </select>
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary p-2" type="submit">Search</button>
                                        </span>
                                        <input type="hidden" name="txtType" value="search" />
                                    </div>
                                </form>
                            </div>

                            <div class="col-sm-4 col-xs-12">

                            </div>
                        </div>

                        <div class="col-sm-2 col-xs-12" style="padding: 0px; ;margin-top: 20px;">
                            <form action="Search" method="post">
                                <div>
                                    <select name="txtSort">
                                        <option>Default</option>
                                        <option value="7" >Reservation ID: ASC</option>
                                        <option value="8" >Reservation ID: DESC</option>
                                        <option value="1" >Date: Lastest</option>
                                        <option value="2" >Date: Oldest</option>       
                                        <option value="3">Total: Low To High</option>
                                        <option value="4">Total: High To Low</option>
                                        <option value="5">Status</option>
                                        <option value="6">Customer Name</option>
                                    </select>
                                    <span class="input-group-btn">
                                        <input type="submit" name="txtType" value="sort"/>
                                    </span>
                                </div>
                            </form>					
                        </div>

                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>    
                                <th>-</th>
                                <th>Reservation ID</th>
                                <th>Check Up Date</th>
                                <th>Patient Name</th>
                                <th>Patient's Children Name</th>
                                <th>Doctor In Charge</th>
                                <th>Total</th>
                                <th>Status</th>

                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="reservation" items="${RESERVATION_LIST}" varStatus="loop">
                            <form action="Patient" method="post">
                                <tr>
                                    <td>${loop.index + 1}</td>
                                    <td>ID: ${reservation.reservationId}</td>
                                    <td> ${reservation.date} </td>
                                    <c:forEach var="patient" items="${PATIENT_LIST}">
                                        <c:if test="${reservation.patientId == patient.accountId}">
                                            <td>${patient.name}</td>
                                        </c:if>
                                    </c:forEach>


                                    <c:forEach var="children" items="${CHILDREN_LIST}">
                                        <c:if test="${reservation.childrenId == children.childrenId}">
                                            <td>${children.name}</td>  
                                        </c:if>
                                    </c:forEach>


                                    <c:forEach var="doctor" items="${DOCTOR_LIST}">
                                        <c:if test="${reservation.staffId == doctor.accountId}">
                                            <td>${doctor.name}</td>  
                                        </c:if>
                                    </c:forEach>

                                    <td class="address-cell">$${reservation.total}</td>
                                    <td>
                                        <c:if test="${reservation.statusId == 1}">
                                            <p style="color: orange; margin-bottom: 0">Pending Confirmation</p>
                                        </c:if>
                                        <c:if test="${reservation.statusId == 2}">
                                            <p style="color: darkgreen; margin-bottom: 0">Confirmed</p>
                                        </c:if>
                                        <c:if test="${reservation.statusId == 3}">
                                            <p style="color: skyblue; margin-bottom: 0">Completed</p>
                                        </c:if>
                                        <c:if test="${reservation.statusId == 4}">
                                            <p style="color: red; margin-bottom: 0">Cancelled</p>
                                        </c:if>
                                        <c:if test="${reservation.statusId == 5}">
                                            <p style="color: purple; margin-bottom: 0">Rescheduled</p>
                                        </c:if>
                                        <c:if test="${reservation.statusId == 6}">
                                            <p style="color: gray; margin-bottom: 0">No Show</p>
                                        </c:if>
                                        <c:if test="${reservation.statusId == 7}">
                                            <p style="color: gray; margin-bottom: 0">Pending Transaction</p>
                                        </c:if>
                                    </td>
                                    <td>        
                                        <a class="deleteBtn" href="ReservationDetail?reservationId=${reservation.reservationId}" style="font-weight: thin; color: skyblue; margin: 3px; background: none; border: none;"">
                                            <i class="fa-solid fa-address-book fa-fade"  style="font-size: 40px;"></i>
                                            <p style="font-size: 15px; font-weight: bold; padding: 0; margin: 0;">MORE</p>
                                        </a>

                                    </td>

                                </tr>
                            </form>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>        
        </div>



    </body>
</html>
