<%-- 
    Document   : service_include
    Created on : Nov 2, 2023, 4:12:27 PM
    Author     : BlackZ36
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            .deleteBtn:hover{
                text-decoration: underline;
            }
        </style>
    </head>
    <body>
        <div class="card-body">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-10 col-xs-12">
                        <h2>Manage <b>Services</b></h2>
                    </div>


                    <div class="col-sm-2 col-xs-12" style="width: 50px;">
                        <a href="ServiceDetail?serviceId=add" class="btn btn-success" >
                            Add New Service
                        </a>						
                    </div>
                </div>



            </div>

            <!-- Table with stripped rows -->
            <table class="table datatable" style="text-align: center;">
                <thead>
                    <tr>
                        <th scope="col">-</th>
                        <th scope="col">Image</th>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Category</th>
                        <th scope="col">Price</th>
                        <th scope="col">Discount</th>
                        <th scope="col">Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody >

                    <c:forEach var="service" items="${SERVICE_LIST}" varStatus="loop">
                        <tr>
                            <th scope="row">${loop.index + 1}</th>
                            <td>
                                <img src="../assets/img/service/${service.image}" alt="alt" width="100" />
                            </td>
                            <td>${service.serviceId}</td>
                            <td>${service.name}</td>
                            <c:forEach var="category" items="${CATEGORY_LIST}">
                                <c:if test="${category.categoryId eq service.categoryId}">
                                    <td>${category.name}</td>
                                </c:if>
                            </c:forEach>

                            <td>${service.price}</td>
                            <td>${service.discount}%</td>
                            <td>${service.status}</td>
                            <td>
                                <a class="button" href="ServiceDetail?serviceId=${service.serviceId}" target="_blank">
                                    <i class="fa-solid fa-pen-to-square" style="font-size: 20px;"></i> Edit
                                </a>
                                </br>
                                <button type="button" class="deleteBtn" name="txtType" style="font-weight: thin; color: red; margin-top: 15px; background: none; border: none;" data-accountid="${service.serviceId}">
                                    <i class="fas fa-trash"  style="font-size: 20px;"></i> Delete
                                </button>
                            </td>
                        </tr>
                    </c:forEach>

                </tbody>
            </table>
            <!-- End Table with stripped rows -->

        </div>
    </div>

    <!--remove modal begin-->
    <div id="deleteEmployeeModal" class="modal fade">
        <div class="modal-dialog" style="margin-top: 180px;">
            <div class="modal-content">
                <form method="post" action="Services">
                    <div class="modal-header">						
                        <div class="form-group">
                            <label>Delete Service ID</label>
                            <input type="text" class="form-control" style="width: 40px" name="txtID" id="IDvaluee" value="" readonly>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">					
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <input type="submit" name="txtType" class="btn btn-danger" value="Delete">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--remove modal end-->
    <script>
        document.querySelectorAll('.deleteBtn').forEach(function (button) {
            button.addEventListener('click', function () {
                var accountId = this.getAttribute('data-accountid');
                // Thiết lập giá trị accountId cho modal

                //change
                document.querySelector('#IDvaluee').value = accountId;
                // Mở modal
                $('#deleteEmployeeModal').modal('show');
            });
        });
        // Bắt sự kiện click của nút "Cancel" và nút "X"
        document.querySelector('#deleteEmployeeModal .modal-footer .btn-default').addEventListener('click', function () {
            // Đóng modal
            $('#deleteEmployeeModal').modal('hide');
        });

        document.querySelector('#deleteEmployeeModal .modal-header .close').addEventListener('click', function () {
            // Đóng modal
            $('#deleteEmployeeModal').modal('hide');
        });
        document.querySelector('#editEmployeeModal .modal-footer .btn-default').addEventListener('click', function () {
            // Đóng modal
            $('#editEmployeeModal').modal('hide');
        });
        // Bắt sự kiện khi người dùng nhấn phím trên bàn phím
        document.addEventListener('keydown', function (event) {
            // Kiểm tra nếu phím nhấn là phím "Esc" (mã 27)
            if (event.keyCode === 27) {
                // Đóng modal khi nhấn phím "Esc"
                $('#deleteEmployeeModal').modal('hide');
            }
        });
    </script>
</body>
</html>
