<%-- 
    Document   : patient_manager_include
    Created on : Oct 19, 2023, 2:56:48 AM
    Author     : duchi
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>

        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            body{
                margin-top:20px;
                background:#eee;
            }
            /* My Account */
            .payments-item img.mr-3 {
                width: 47px;
            }
            .order-list .btn {
                border-radius: 2px;
                min-width: 121px;
                font-size: 13px;
                padding: 7px 0 7px 0;
            }
            .osahan-account-page-left .nav-link {
                padding: 18px 20px;
                border: none;
                font-weight: 600;
                color: #535665;
            }
            .osahan-account-page-left .nav-link i {
                width: 28px;
                height: 28px;
                background: #535665;
                display: inline-block;
                text-align: center;
                line-height: 29px;
                font-size: 15px;
                border-radius: 50px;
                margin: 0 7px 0 0px;
                color: #fff;
            }
            .osahan-account-page-left .nav-link.active {
                background: #f3f7f8;
                color: #282c3f !important;
            }
            .osahan-account-page-left .nav-link.active i {
                background: #282c3f !important;
            }
            .osahan-user-media img {
                width: 90px;
            }
            .card offer-card h5.card-title {
                border: 2px dotted #000;
            }
            .card.offer-card h5 {
                border: 1px dotted #daceb7;
                display: inline-table;
                color: #17a2b8;
                margin: 0 0 19px 0;
                font-size: 15px;
                padding: 6px 10px 6px 6px;
                border-radius: 2px;
                background: #fffae6;
                position: relative;
            }
            .card.offer-card h5 img {
                height: 22px;
                object-fit: cover;
                width: 22px;
                margin: 0 8px 0 0;
                border-radius: 2px;
            }
            .card.offer-card h5:after {
                border-left: 4px solid transparent;
                border-right: 4px solid transparent;
                border-bottom: 4px solid #daceb7;
                content: "";
                left: 30px;
                position: absolute;
                bottom: 0;
            }
            .card.offer-card h5:before {
                border-left: 4px solid transparent;
                border-right: 4px solid transparent;
                border-top: 4px solid #daceb7;
                content: "";
                left: 30px;
                position: absolute;
                top: 0;
            }
            .payments-item .media {
                align-items: center;
            }
            .payments-item .media img {
                margin: 0 40px 0 11px !important;
            }
            .reviews-members .media .mr-3 {
                width: 56px;
                height: 56px;
                object-fit: cover;
            }
            .order-list img.mr-4 {
                width: 70px;
                height: 70px;
                object-fit: cover;
                box-shadow: 0 .125rem .25rem rgba(0, 0, 0, .075)!important;
                border-radius: 2px;
            }
            .osahan-cart-item p.text-gray.float-right {
                margin: 3px 0 0 0;
                font-size: 12px;
            }
            .osahan-cart-item .food-item {
                vertical-align: bottom;
            }

            .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
                color: #000000;
            }

            .shadow-sm {
                box-shadow: 0 .125rem .25rem rgba(0,0,0,.075)!important;
            }

            .rounded-pill {
                border-radius: 50rem!important;
            }
            a:hover{
                text-decoration:none;
            }
        </style>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="osahan-account-page-left shadow-sm bg-white h-100">
                        <div class="border-bottom p-4" style="background-color: #ecf8f9;">
                            <div class="osahan-user text-center">
                                <div class="osahan-user-media">
                                    <h5 class="mb-2">Doctor In Charge</h5>
                                    <img class="mb-3 rounded-4 shadow-sm mt-3" src="../assets/img/user/${DOCTOR.username}/${DOCTOR.avatar}" alt="doctor avatar">
                                    <div class="osahan-user-media-body">
                                        <h6 class="mb-2">Name: ${DOCTOR.name}</h6>
                                        <p class="mb-1">Phone: ${DOCTOR.phone}</p>
                                        <p>Email: ${DOCTOR.email}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="nav nav-tabs flex-column border-0 pl-4 pb-4" id="myTab" role="tablist">
                            <div class="border-bottom p-4">
                                <div class="osahan-user text-center">
                                    <div class="osahan-user-media">
                                        <h5 class="mb-2">Patient's Children Information</h5>
                                        <div class="osahan-user-media-body">
                                            <h6 class="mb-2">Name: ${CHILDREN.name}</h6>
                                            <p class="mb-1">
                                                <c:if test="${CHILDREN.gender == true}">
                                                    Gender: Boy
                                                </c:if>
                                                <c:if test="${CHILDREN.gender == false}">
                                                    Gender: Girl
                                                </c:if>
                                            </p>
                                            <p class="mb-1" > Date Of Birth: ${CHILDREN.dob}</p>
                                            <p>
                                                <c:if test="${CHILDREN.relation == 1}">
                                                    Relationship: Patient's Son
                                                </c:if>
                                                <c:if test="${CHILDREN.relation == 2}">
                                                    Relationship: Patient's Daughter
                                                </c:if>
                                                <c:if test="${CHILDREN.relation == 3}">
                                                    Relationship: Other
                                                </c:if>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ul>
                        <ul class="nav nav-tabs flex-column border-0 pl-4 pb-4" id="myTab" role="tablist">
                            <div class="p-4">
                                <div class="osahan-user text-center">
                                    <div class="osahan-user-media">
                                        <h5 class="mb-2">Patient's Information</h5>
                                        <img class="mb-3 rounded-4 shadow-sm mt-3" src="../assets/img/user/${PATIENT.username}/${PATIENT.avatar}" alt="customer avatar">
                                        <div class="osahan-user-media-body">
                                            <h6 class="mb-2">Name: ${PATIENT.name}</h6>
                                            <p class="mb-1">
                                                <c:if test="${PATIENT.gender == true}">
                                                    Gender: Male
                                                </c:if>
                                                <c:if test="${PATIENT.gender == false}">
                                                    Gender: Female
                                                </c:if>
                                            </p>
                                            <p class="mb-1">Phone: ${PATIENT.phone}</p>
                                            <p>Email: ${PATIENT.email}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="osahan-account-page-right shadow-sm bg-white p-4 h-100">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade  active show" id="favourites" role="tabpanel" aria-labelledby="favourites-tab">

                                <div class="row">

                                    <h4 class="font-weight-bold mt-4 mb-5 text-center">My Reservation Information</h4>
                                    <div class="col-md-12 text-left load-more border-bottom mb-5">
                                        <div class="mb-5" >
                                            <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                <i class="fa-solid fa-list-check" style="margin-right: 10px;"></i>
                                                <span class="mr-3" style="width: 200px;">Reservation ID:</span>
                                                <input style="width: 300px; background-color: #f2f2f2;" class="form-control" name="txtID" value="${RESERVATION.reservationId}" readonly/>
                                            </div> 
                                            <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                <i class="fa-regular fa-calendar-days" style="margin-right: 10px;"></i>
                                                <span class="mr-3" style="width: 200px;">Booked Date:</span>
                                                <input style="width: 300px; background-color: #f2f2f2;" class="form-control" value="${RESERVATION.bookDate}" readonly/>
                                            </div>
                                            <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                <i class="fa-regular fa-calendar-days" style="margin-right: 10px;"></i>
                                                <span class="mr-3" style="width: 200px; font-weight: bold;">Check Up Date:</span>
                                                <input style="width: 300px; background-color: #f2f2f2; font-weight: bold;" class="form-control" value="${RESERVATION.date}" readonly/>
                                            </div>
                                            <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                <i class="fa-regular fa-calendar-days" style="margin-right: 10px;"></i>
                                                <span class="mr-3" style="width: 200px; font-weight: bold;">Slot Number:</span>
                                                <input style="width: 300px; background-color: #f2f2f2; font-weight: bold;" class="form-control" value="${RESERVATION.slotId}" readonly/>
                                            </div>
                                            <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                <i class="fa-regular fa-clock" style="margin-right: 10px;"></i>
                                                <span class="mr-3" style="width: 200px; font-weight: bold;">Time:</span>
                                                <c:if test="${RESERVATION.slotId == 1}">
                                                    <input style="width: 300px; background-color: #f2f2f2; font-weight: bold;" class="form-control" value="7:30 - 9:30 (AM)" readonly/>
                                                </c:if>
                                                <c:if test="${RESERVATION.slotId == 2}">
                                                    <input style="width: 300px; background-color: #f2f2f2; font-weight: bold;" class="form-control" value="9:30 - 11:30 (AM)" readonly/>
                                                </c:if>
                                                <c:if test="${RESERVATION.slotId == 3}">
                                                    <input style="width: 300px; background-color: #f2f2f2; font-weight: bold;" class="form-control" value="14:30 - 16:30 (PM)" readonly/>
                                                </c:if>
                                                <c:if test="${RESERVATION.slotId == 4}">
                                                    <input style="width: 300px; background-color: #f2f2f2; font-weight: bold;" class="form-control" value="16:30 - 18:30 (PM)" readonly/>
                                                </c:if>

                                            </div>
                                            <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                <i class="fa-solid fa-file-invoice-dollar" style="margin-right: 10px;"></i>
                                                <span class="mr-3" style="width: 200px;">Total:</span>
                                                <input style="width: 300px; background-color: #f2f2f2;" class="form-control" value="$${RESERVATION.total}" readonly/>
                                            </div>
                                            <form action="ManagerUpdate" method="post">
                                                <div class=" d-flex flex-row align-items-center mt-3" style="margin-left: 100px;">
                                                    <i class="fa-solid fa-hand-holding-medical" style="margin-right: 10px;"></i>
                                                    <span class="mr-3" style="width: 200px;">Status:</span>
                                                    <select name="txtStatus" style="width: 300px; background-color: #ecf8f9;" class="form-control">
                                                        <option value="1" ${RESERVATION.statusId eq 1 ? 'selected' : ''}>Pending Confirmation</option>
                                                        <option value="2" ${RESERVATION.statusId eq 2 ? 'selected' : ''}>Confirmed</option>
                                                        <option value="3" ${RESERVATION.statusId eq 3 ? 'selected' : ''}>Completed</option>
                                                        <option value="4" ${RESERVATION.statusId eq 4 ? 'selected' : ''}>Cancelled</option>
                                                        <option value="5" ${RESERVATION.statusId eq 5 ? 'selected' : ''}>Rescheduled</option>
                                                        <option value="6" ${RESERVATION.statusId eq 6 ? 'selected' : ''}>No Show</option>
                                                        <option value="7" ${RESERVATION.statusId eq 7 ? 'selected' : ''}>Pending Transaction</option>
                                                    </select>
                                                </div>
                                        </div>
                                        <!--button zone start-->
                                        <div class="col-md-12 text-center load-more mb-4" style="">
                                            <button type="submit" class="noshowBtn" name="txtID" value="${RESERVATION.reservationId}" style=" padding: 10px; background-color: #ecf8f9; border: 1px; border-radius: 5px; color: black; text-decoration: none; display: inline-block;">
                                                <i class="fa-solid fa-phone-slash" style="font-size: 20px;"></i>
                                                Update Status
                                            </button>
                                        </div>
                                        </form>
                                        <!--button zone end-->
                                    </div>



                                    <h4 class="font-weight-bold mt-0 mb-5 text-center">Selected Medical Service(s)</h4>
                                    <c:forEach  var="detail" items="${RESERVATION_DETAIL}">
                                        <c:forEach var="service" items="${SERVICE_LIST}">
                                            <c:if test="${service.serviceId eq detail.serviceId}">
                                                <div class="col-md-4 col-sm-6 mb-4 pb-2">
                                                    <div class="list-card bg-white h-100 rounded overflow-hidden position-relative shadow-sm">
                                                        <div class="list-card-image">
                                                            <a href="ServiceDetail?serviceID=${service.serviceId}" target="_blank">
                                                                <img src="../assets/img/service/${service.image}" class="img-fluid item-img w-100">
                                                            </a>
                                                        </div>
                                                        <div class="p-3 position-relative">
                                                            <div class="list-card-body">
                                                                <h6 class="mb-1">
                                                                    <a href="ServiceDetail?serviceID=${service.serviceId}" target="_blank" class="text-black">${service.name}</a>
                                                                </h6>
                                                                <p class="text-gray mb-3 time"><span class="bg-light text-dark rounded-sm pl-2 pb-1 pt-1 pr-2"><i class="fa-solid fa-money-bill-wave"></i> ${detail.price}$</span></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:if>
                                        </c:forEach>
                                    </c:forEach>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </body>
</html>
