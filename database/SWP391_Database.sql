﻿USE master
GO

ALTER DATABASE SWP391_Database SET SINGLE_USER WITH ROLLBACK IMMEDIATE 

DROP DATABASE SWP391_Database
GO

-- Tạo database mới
CREATE DATABASE SWP391_Database;
GO

-- Sử dụng database mới
USE SWP391_Database;
GO




-- Tạo bảngStatus
CREATE TABLE Status (
    status_id INT IDENTITY(1,1) PRIMARY KEY,
    status_name NVARCHAR(50) NULL
);

-- Tạo bảng Reservation Status
CREATE TABLE ReservationStatus (
    status_id INT IDENTITY(1,1) PRIMARY KEY,
    status_name NVARCHAR(50) NULL
);

-- Tạo bảng Role
CREATE TABLE Role (
    role_id INT IDENTITY(1,1) PRIMARY KEY,
    role_name NVARCHAR(50) NULL
);


-- Tạo bảng Feature
CREATE TABLE Feature (
	feature_id INT IDENTITY(1,1) PRIMARY KEY,
	name NVARCHAR(100) NULL,
	url NVARCHAR(100) NULL,
	note NVARCHAR(100) NULL
);

-- Tạo bảng RoleFeature
CREATE TABLE RoleFeature (
	feature_id INT,
	role_id INT,
	FOREIGN KEY (feature_id) REFERENCES Feature(feature_id),
	FOREIGN KEY (role_id) REFERENCES Role(role_id)
)

-- Tạo bảng Patient
CREATE TABLE Patient (
    patient_id INT IDENTITY(1,1) PRIMARY KEY,
    username NVARCHAR(25) NULL,
    password NVARCHAR(25) NULL,
    email NVARCHAR(100) NULL,
    phone NVARCHAR(15) NULL,
    name NVARCHAR(50) NULL,
    gender BIT NULL,
    avatar NVARCHAR(100) NULL,
    address NVARCHAR(100),
	role_id INT NULL,
	status_id INT NULL,
    FOREIGN KEY (status_id) REFERENCES Status(status_id),
	FOREIGN KEY (role_id) REFERENCES Role(role_id)
);

-- Tạo bảng Children
CREATE TABLE Children(
	children_id INT IDENTITY(1,1) PRIMARY KEY,
	parent_id INT,
	name NVARCHAR(100) NULL,
	gender BIT NULL,
	dob DATE NULL,
	Relation INT NULL,
	FOREIGN KEY (parent_id) REFERENCES Patient(patient_id)
);


-- Tạo bảng Staff
CREATE TABLE Staff (
    staff_id INT IDENTITY(1,1) PRIMARY KEY,
    username NVARCHAR(25) NULL,
    password NVARCHAR(25) NULL,
    email NVARCHAR(100) NULL,
    phone NVARCHAR(15) NULL,
    name NVARCHAR(50) NULL,
    gender BIT NULL,
    avatar NVARCHAR(100) NULL,
	address NVARCHAR(100),
    role_id INT NULL,
    status_id INT NULL,
    FOREIGN KEY (role_id) REFERENCES Role(role_id),
    FOREIGN KEY (status_id) REFERENCES Status(status_id)
);

-- Tạo bảng Admin
CREATE TABLE Admin (
    admin_id INT IDENTITY(1,1) PRIMARY KEY,
    username NVARCHAR(50) NULL,
    password NVARCHAR(50) NULL,
);


-- Tạo bảng Service Category
CREATE TABLE Service_category (
    category_id INT IDENTITY(1,1) PRIMARY KEY,
    name VARCHAR(255) NULL,
	detail NVARCHAR(255) NULL
);

-- Tạo bảng Service
CREATE TABLE Service (
    service_id INT IDENTITY(1,1) PRIMARY KEY,
	category_id INT NULL,
    image VARCHAR(255) NULL,
    name VARCHAR(255) NULL,
    detail TEXT NULL,
    price DECIMAL(10, 2) NULL,
    discount DECIMAL(5, 2) NULL,
	status_id INT NULL
    FOREIGN KEY (category_id) REFERENCES Service_category(category_id)
);


-- Tạo bảng Post
CREATE TABLE Post (
    post_id INT IDENTITY(1,1) PRIMARY KEY,
    title NVARCHAR(200) NULL,
    author_id INT NULL,
    updated_date DATETIME NULL,
    detail NVARCHAR(MAX) NULL,
    image NVARCHAR(200) NULL,
    FOREIGN KEY (author_id) REFERENCES Staff(staff_id)
);



-- Tạo bảng Slot
CREATE TABLE Slot (
    slot_id INT IDENTITY(1,1) PRIMARY KEY,
    start_time NVARCHAR(25) NULL,
    end_time NVARCHAR(25) NULL
);

-- Tạo bảng Reservation
CREATE TABLE Reservation (
    reservation_id INT IDENTITY(1,1) PRIMARY KEY,
    slot_id INT NULL,
    patient_id INT NULL,
    children_id INT NULL,
    staff_id INT NULL,
    status_id INT NULL,
	book_date Date NULL,
    date DATE NULL,
    total DECIMAL(10, 2) NULL,
    FOREIGN KEY (slot_id) REFERENCES Slot(slot_id),
    FOREIGN KEY (patient_id) REFERENCES Patient(patient_id),
    FOREIGN KEY (children_id) REFERENCES Children(children_id), -- Liên kết với thông tin trẻ em
    FOREIGN KEY (staff_id) REFERENCES Staff(staff_id),
    FOREIGN KEY (status_id) REFERENCES ReservationStatus(status_id)
);

-- Tạo bảng Reservation_detail
CREATE TABLE Reservation_detail (
    reservation_detail_id INT IDENTITY(1,1) PRIMARY KEY,
    reservation_id INT NULL,
    service_id INT NULL,
    price DECIMAL(10, 2) NULL,
    FOREIGN KEY (reservation_id) REFERENCES Reservation(reservation_id),
    FOREIGN KEY (service_id) REFERENCES Service(service_id),
);

-- Tạo bảng activation
CREATE TABLE EmailVerification (
    activation_id INT  IDENTITY(1,1) PRIMARY KEY,
	email NVARCHAR(30) Null,
    beginTime DATETIME Null,
    endTime DATETIME Null,
	type int NULL,
    status int Null
);

-- Tạo bảng Feedback
CREATE TABLE Feedback (
    feedback_id INT IDENTITY(1,1) PRIMARY KEY,
	service_id INT NULL,
	patient_id INT NULL,
    rate INT NULL,
	title NVARCHAR(255) NULL,
	detail NVARCHAR(MAX) NULL,
	update_date DATETIME NULL,
	FOREIGN KEY (service_id) REFERENCES Service(service_id),
	FOREIGN KEY (patient_id) REFERENCES Patient(patient_id)
);


-- Thêm dữ liệu vào bảng Role
INSERT INTO [dbo].[Role] ([role_name]) VALUES
( N'patient'),
( N'nurse'),
( N'doctor'),
( N'manager');
GO


-- Thêm dữ liệu vào bảng StaffStatus
INSERT INTO Status (status_name) VALUES 
(N'Pending'),       -- Hoạt động
(N'Active'),     -- Không hoạt động
(N'Deactive');    -- Tạm ngưng hoạt động

-- Thêm dữ liệu vào bảng Reservation Status
INSERT INTO ReservationStatus (status_name) VALUES 
(N'Pending Confirmation'),
(N'Confirmed'),
(N'Completed'),
(N'Cancelled'),
(N'Rescheduled'),
(N'No Show'),
(N'Pending Transaction');

-- Thêm dữ liệu vào bảng Staff (1 nurse, 2 doctor, 1 manager)
INSERT INTO Staff ( username, password, email, phone, name, gender, avatar, address, role_id, status_id) VALUES 
(N'nurse', N'123', N'nurse1@example.com', N'1111111111', N'Nurse 1', 0, N'default.jpg',N'address default', 2, 2), -- Nurse
(N'doctor1', N'123', N'amanda.johnson@example.com', N'123-456-7890', N'Dr. Amanda Johnson', 1, N'doctors-2.jpg',N'123 Health Street, Medical City, Medicalville, MD 12345, USA', 3, 2), -- Doctor
(N'doctor2', N'123', N' daniel.lee@example.com', N'987-654-3210', N'Dr. Daniel Lee', 1, N'doctors-1.jpg',N'456 Care Avenue, Health Town, Medicland, MD 67890, USA', 3, 2), -- Doctor
(N'doctor3', N'123', N'sarah.smith@example.com', N'111-222-3333', N'Dr. Sarah Smith', 1, N'doctors-4.jpg',N'789 Medical Boulevard, Wellness City, Healthland, MD 24680, USA', 3, 2), -- Doctor
(N'manager', N'123', N'manager1@example.com', N'3333333333', N'Manager 1', 1, N'default.jpg',N'address default', 4, 2), -- Manager
(N'doctor4', N'123', N'michael.brown@example.com', N'333-555-7777', N'Dr. Michael Brown', 1, N'doctors-3.jpg',N'101 Hospital Lane, Careville, Health Springs, MD 13579, USA', 3, 2); -- Doctor

-- Thêm dữ liệu vào bảng Patient với các trạng thái từ bảng PatientStatus
INSERT INTO Patient (username, password, email, phone, name, gender, avatar, address, role_id, status_id) VALUES 
( N'p1', N'123', N'newpatient1@example.com', N'1112223333', N'John Smith', 1, N'default.jpg', N'123 Main St, City1, State1, 12345', 1, 2),
( N'p2', N'123', N'newpatient2@example.com', N'2223334444', N'Alice Johnson', 0, N'default.jpg', N'456 Elm St, City2, State2, 23456', 1, 2),
( N'p3', N'123', N'newpatient3@example.com', N'3334445555', N'Michael Davis', 1, N'default.jpg', N'789 Oak St, City3, State3, 34567', 1, 2),
( N'p4', N'123', N'newpatient4@example.com', N'4445556666', N'Emily Wilson', 1, N'default.jpg', N'101 Pine St, City4, State4, 45678', 1, 2),
( N'p5', N'123', N'newpatient5@example.com', N'5556667777', N'David Brown', 0, N'default.jpg', N'202 Maple St, City5, State5, 56789', 1, 1),
( N'p6', N'123', N'newpatient6@example.com', N'6667778888', N'Sophia Lee', 1, N'default.jpg', N'303 Cedar St, City6, State6, 67890', 1, 3);


-- Thêm dữ liệu vào bảng feature
INSERT INTO Feature (name, url, note) VALUES
(N'CreateReservation', N'/Reservation/Create', N'A website to create Reservations'),
(N'BookReservation', N'/Reservation/Book', N'A website to book Reservations')

-- Thêm dữ liệu cho phép manager truy cập vào CreateReservation, patient vào BookReservation
INSERT INTO RoleFeature (role_id, feature_id) VALUES
(4, 1), (1,2)

--Thêm dữ liệu 4 slot vào
INSERT INTO Slot (start_time, end_time) VALUES
(N'07:30 AM', N'9:30 AM'), (N'09:30 AM', N'11:30 AM'), (N'02:30 PM', N'4:30 PM'), (N'04:30 PM', N'6:30 PM')

-- Chèn dữ liệu vào bảng category
INSERT INTO Service_category( name, detail)
VALUES
( 'Health Consultation', N'Health Consultation Detail'), 
( 'Medical Examination', N'Health Consultation Detail'), 
( 'Vaccination', N'Health Consultation Detail');


-- Chèn dữ liệu vào bảng service
INSERT INTO service ( category_id, image, name, detail, price, discount, status_id )
VALUES
-- Dịch vụ cho Health Consultation (category_id = 1)
( 1, 'default.jpg', 'Health Checkup', 'Regular health checkup and consultation.', 100.00, 5.00,1),
( 1, 'default.jpg', 'Diet Advice', 'Nutritional consultation and personalized diet planning.', 100.00, 10.00,1),
( 1, 'default.jpg', 'Fitness Program', 'Customized fitness program and exercise routines.', 100.000, 15.00,1),

-- Dịch vụ cho Medical Examination (category_id = 2)
(2, 'default.jpg', 'General Medical Examination', 'Comprehensive medical examination for overall health assessment.', 100.00, 5.00,1),
(2, 'default.jpg', 'Blood Tests', 'Various blood tests to assess specific health parameters.', 100.00, 10.00,1),
(2, 'default.jpg', 'X-ray Services', 'X-ray imaging for diagnostic purposes.', 100.00, 15.00, 1),

-- Dịch vụ cho Vaccination (category_id = 3)
(3, 'default.jpg', 'Flu Vaccination', 'Annual flu vaccination to prevent influenza.', 100.00, 5.00, 1),
(3, 'default.jpg', 'Childhood Vaccination', 'Routine vaccinations for children as per vaccination schedule.', 100.00, 10.00, 1),
(3, 'default.jpg', 'Travel Vaccination', 'Vaccinations required for travel to specific regions.', 100.00, 15.00, 1);


-- Thêm feedback 
INSERT INTO Feedback (service_id, patient_id, rate, title, detail, update_date)
VALUES
-- Dịch vụ từ ID 1 đến 3 (Health Consultation)
(1, 1, 5, 'Excellent Service', 'The health checkup was thorough and professional.','10-10-23'),
(2, 1, 4, 'Good Diet Advice', 'The diet advice was helpful and personalized.','10-10-23'),
(3, 1, 3, 'Average Fitness Program', 'The fitness program was okay, could be more tailored to my needs.','10-10-23'),

-- Dịch vụ từ ID 4 đến 6 (Medical Examination)
(4, 1, 5, 'Comprehensive Medical Examination', 'The medical examination covered all aspects of my health.','10-10-23'),
(5, 1, 4, 'Accurate Blood Tests', 'The blood tests were accurate and results were delivered promptly.','10-10-23'),
(6, 1, 3, 'X-ray Service', 'The X-ray service was satisfactory, no issues reported.','10-10-23'),

-- Dịch vụ từ ID 7 đến 9 (Vaccination)
(7, 1, 5, 'Flu Vaccination', 'The flu vaccination was administered efficiently.','10-10-23'),
(8, 1, 4, 'Childhood Vaccination', 'My child received the vaccinations as scheduled, no complaints.','10-10-23'),
(9, 1, 3, 'Travel Vaccination', 'The travel vaccinations were done adequately, but the waiting time was long.','10-10-23');
GO

-- Thêm một dòng dữ liệu vào bảng admin
INSERT INTO Admin (username,password)
VALUES ('admin','admin');

-- Thêm một dòng dữ liệu vào bảng Children
INSERT INTO Children (parent_id, name, gender, dob, Relation) VALUES 
	(1, N'Nguyễn Văn A', 1, '2000-01-15', 1),
	(1, N'Nguyễn Thị B', 0, '2000-01-15', 2),
	(1, N'Nguyễn Văn C', 1, '2000-01-15', 3),
	(2, N'Nguyễn Thị D', 0, '2000-01-15', 1),
	(2, N'Nguyễn Văn E', 1, '2000-01-15', 2),
	(2, N'Nguyễn Thị F', 0, '2000-01-15', 3);

GO
-- Thêm 10 bản ghi vào bảng Post

INSERT INTO Post (title, author_id, updated_date, detail, image) VALUES 
('Top 10 Health Tips', 2, DATEADD(DAY, -10, GETDATE()), 'Here are the top 10 health tips for a healthy lifestyle. Incorporating these tips into your daily routine can significantly improve your overall well-being and longevity. From balanced nutrition to regular exercise and stress management, these tips cover a wide range of aspects essential for a healthy life.', 'health_tips.jpg'),
('Benefits of Regular Exercise', 4, DATEADD(DAY, -9, GETDATE()), 'Regular exercise has numerous health benefits including weight management, improved cardiovascular health, and enhanced mood. This post explores the various advantages of incorporating regular physical activity into your routine.', 'exercise_benefits.jpg'),
('Healthy Eating Habits', 3, DATEADD(DAY, -8, GETDATE()), 'Learn about healthy eating habits and how to maintain a balanced diet. Discover the importance of consuming a variety of nutrients and the impact of your food choices on your overall health and vitality.', 'healthy_eating.jpg'),
('Managing Stress Effectively', 2, DATEADD(DAY, -7, GETDATE()), 'Discover effective ways to manage stress in your daily life. Stress management techniques, such as meditation, deep breathing exercises, and time management strategies, can help you cope with life’s challenges and improve your mental well-being.', 'stress_management.jpg'),
('Importance of Mental Health', 4, DATEADD(DAY, -6, GETDATE()), 'Understanding the importance of mental health and how it influences your overall well-being. This post discusses mental health disorders, the significance of seeking help, and the impact of mental well-being on your daily life.', 'mental_health.jpg'),
('Preventing Common Illnesses', 3, DATEADD(DAY, -5, GETDATE()), 'Tips on preventing common illnesses and staying healthy. From practicing good hygiene to getting vaccinated and maintaining a balanced diet, these preventive measures can protect you from various diseases and infections.', 'illness_prevention.jpg'),
('The Role of Sleep in Health', 2, DATEADD(DAY, -4, GETDATE()), 'Explore the importance of sleep in maintaining overall health. Quality sleep is essential for physical and mental restoration, immune function, and overall well-being. Learn about the optimal sleep duration and effective sleep hygiene practices.', 'sleep_health.jpg'),
('Healthy Heart Tips', 4, DATEADD(DAY, -3, GETDATE()), 'Learn how to keep your heart healthy with these essential tips. From cardiovascular exercises to heart-friendly nutrition and stress reduction techniques, these practices can help you maintain a strong and healthy heart.', 'heart_health.jpg'),
('Boosting Immune System', 3, DATEADD(DAY, -2, GETDATE()), 'Ways to boost your immune system and protect against diseases. A robust immune system is crucial for fighting infections and staying healthy. This post covers immune-boosting foods, lifestyle habits, and supplements that can enhance your immune response.', 'immune_system.jpg'),
('Managing Chronic Conditions', 2, DATEADD(DAY, -1, GETDATE()), 'Strategies for managing chronic health conditions and improving your quality of life. Whether you have diabetes, hypertension, or other chronic illnesses, these tips can help you effectively manage your condition, minimize symptoms, and lead a fulfilling life.', 'chronic_conditions.jpg');


GO

-- Thêm các reservation có sẵn
INSERT INTO Reservation (slot_id, patient_id, children_id, staff_id, status_id, book_date, date, total) VALUES 
(1, 1, 1, 4, 1,'2023-10-25' ,'2023-10-27', 270.00), 
(1, 1, 3, 4, 1,'2023-10-25' ,'2023-10-28', 270.00);
GO

-- Thêm chi tiết đặt lịch hẹn (Reservation_detail) cho reservation_id vừa thêm vào
INSERT INTO Reservation_detail (reservation_id, service_id, price)
VALUES 
(1, 1, 95), 
(1, 2, 90), 
(1, 3, 85); 
GO



ALTER DATABASE SWP391_Database SET MULTI_USER
GO

